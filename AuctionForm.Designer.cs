﻿namespace AukcioKliens
{
    partial class AuctionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AuctionFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.AuctionNameTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.AuctionCategoryTextBox = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.AuctionFeedbackFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel22 = new System.Windows.Forms.FlowLayoutPanel();
            this.label17 = new System.Windows.Forms.Label();
            this.AuctionFeedbackPointComboBox = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.AuctionFeedbackDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.AuctionSendFeedbackButton = new System.Windows.Forms.Button();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
            this.AuctionLicit1FlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.AuctionPriceNowTextBox = new System.Windows.Forms.TextBox();
            this.AuctionLicit2FlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label15 = new System.Windows.Forms.Label();
            this.AuctionBidTextBox = new System.Windows.Forms.TextBox();
            this.AuctionMaxBidFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.AuctionMAxBidTextBox = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel21 = new System.Windows.Forms.FlowLayoutPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.AuctionMinPriceTextBox = new System.Windows.Forms.TextBox();
            this.PlaceholderLabel = new System.Windows.Forms.Label();
            this.AuctionBidButton = new System.Windows.Forms.Button();
            this.AuctionWinnerFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.AuctionStartDateFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.AuctionStartDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.flowLayoutPanel19 = new System.Windows.Forms.FlowLayoutPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.AuctionEndDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
            this.AuctionPictureBox = new System.Windows.Forms.PictureBox();
            this.AuctionGetPictureFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.AuctionBrowsePictureButton = new System.Windows.Forms.Button();
            this.AuctionDescriptionFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.AuctionDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.AuctionQuestionFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label16 = new System.Windows.Forms.Label();
            this.AuctionQuestionListBox = new System.Windows.Forms.ListBox();
            this.flowLayoutPanel26 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.AuctionSaveButton = new System.Windows.Forms.Button();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel24 = new System.Windows.Forms.FlowLayoutPanel();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.EmailFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.EmailTextBox = new System.Windows.Forms.TextBox();
            this.EmailLabel = new System.Windows.Forms.Label();
            this.AuctionBidOwnerTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.AuctionFlowLayoutPanel.SuspendLayout();
            this.flowLayoutPanel9.SuspendLayout();
            this.flowLayoutPanel11.SuspendLayout();
            this.AuctionFeedbackFlowLayoutPanel.SuspendLayout();
            this.flowLayoutPanel22.SuspendLayout();
            this.flowLayoutPanel13.SuspendLayout();
            this.flowLayoutPanel15.SuspendLayout();
            this.AuctionLicit1FlowLayoutPanel.SuspendLayout();
            this.flowLayoutPanel10.SuspendLayout();
            this.AuctionLicit2FlowLayoutPanel.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.AuctionMaxBidFlowLayoutPanel.SuspendLayout();
            this.flowLayoutPanel21.SuspendLayout();
            this.AuctionWinnerFlowLayoutPanel.SuspendLayout();
            this.flowLayoutPanel17.SuspendLayout();
            this.AuctionStartDateFlowLayoutPanel.SuspendLayout();
            this.flowLayoutPanel19.SuspendLayout();
            this.flowLayoutPanel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AuctionPictureBox)).BeginInit();
            this.AuctionGetPictureFlowLayoutPanel.SuspendLayout();
            this.AuctionDescriptionFlowLayoutPanel.SuspendLayout();
            this.AuctionQuestionFlowLayoutPanel.SuspendLayout();
            this.flowLayoutPanel26.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            this.EmailFlowLayoutPanel.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // AuctionFlowLayoutPanel
            // 
            this.AuctionFlowLayoutPanel.Controls.Add(this.flowLayoutPanel9);
            this.AuctionFlowLayoutPanel.Controls.Add(this.flowLayoutPanel11);
            this.AuctionFlowLayoutPanel.Controls.Add(this.AuctionDescriptionFlowLayoutPanel);
            this.AuctionFlowLayoutPanel.Controls.Add(this.AuctionQuestionFlowLayoutPanel);
            this.AuctionFlowLayoutPanel.Controls.Add(this.flowLayoutPanel26);
            this.AuctionFlowLayoutPanel.Controls.Add(this.flowLayoutPanel24);
            this.AuctionFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.AuctionFlowLayoutPanel.Location = new System.Drawing.Point(0, 30);
            this.AuctionFlowLayoutPanel.Name = "AuctionFlowLayoutPanel";
            this.AuctionFlowLayoutPanel.Size = new System.Drawing.Size(817, 750);
            this.AuctionFlowLayoutPanel.TabIndex = 21;
            this.AuctionFlowLayoutPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panels_MouseDown);
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.Controls.Add(this.label7);
            this.flowLayoutPanel9.Controls.Add(this.AuctionNameTextBox);
            this.flowLayoutPanel9.Controls.Add(this.label8);
            this.flowLayoutPanel9.Controls.Add(this.AuctionCategoryTextBox);
            this.flowLayoutPanel9.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(814, 91);
            this.flowLayoutPanel9.TabIndex = 0;
            this.flowLayoutPanel9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panels_MouseDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Termék neve:";
            // 
            // AuctionNameTextBox
            // 
            this.AuctionNameTextBox.Location = new System.Drawing.Point(3, 16);
            this.AuctionNameTextBox.Name = "AuctionNameTextBox";
            this.AuctionNameTextBox.Size = new System.Drawing.Size(645, 20);
            this.AuctionNameTextBox.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 39);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Kategória";
            // 
            // AuctionCategoryTextBox
            // 
            this.AuctionCategoryTextBox.Location = new System.Drawing.Point(3, 55);
            this.AuctionCategoryTextBox.Name = "AuctionCategoryTextBox";
            this.AuctionCategoryTextBox.Size = new System.Drawing.Size(197, 20);
            this.AuctionCategoryTextBox.TabIndex = 4;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.Controls.Add(this.AuctionFeedbackFlowLayoutPanel);
            this.flowLayoutPanel11.Controls.Add(this.flowLayoutPanel13);
            this.flowLayoutPanel11.Controls.Add(this.flowLayoutPanel14);
            this.flowLayoutPanel11.Location = new System.Drawing.Point(3, 100);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(814, 303);
            this.flowLayoutPanel11.TabIndex = 2;
            this.flowLayoutPanel11.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panels_MouseDown);
            // 
            // AuctionFeedbackFlowLayoutPanel
            // 
            this.AuctionFeedbackFlowLayoutPanel.Controls.Add(this.flowLayoutPanel22);
            this.AuctionFeedbackFlowLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.AuctionFeedbackFlowLayoutPanel.Name = "AuctionFeedbackFlowLayoutPanel";
            this.AuctionFeedbackFlowLayoutPanel.Size = new System.Drawing.Size(200, 133);
            this.AuctionFeedbackFlowLayoutPanel.TabIndex = 1;
            // 
            // flowLayoutPanel22
            // 
            this.flowLayoutPanel22.Controls.Add(this.label17);
            this.flowLayoutPanel22.Controls.Add(this.AuctionFeedbackPointComboBox);
            this.flowLayoutPanel22.Controls.Add(this.label18);
            this.flowLayoutPanel22.Controls.Add(this.AuctionFeedbackDescriptionTextBox);
            this.flowLayoutPanel22.Controls.Add(this.AuctionSendFeedbackButton);
            this.flowLayoutPanel22.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel22.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel22.Name = "flowLayoutPanel22";
            this.flowLayoutPanel22.Size = new System.Drawing.Size(195, 126);
            this.flowLayoutPanel22.TabIndex = 3;
            this.flowLayoutPanel22.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panels_MouseDown);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(87, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Értékelés értéke:";
            // 
            // AuctionFeedbackPointComboBox
            // 
            this.AuctionFeedbackPointComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AuctionFeedbackPointComboBox.FormattingEnabled = true;
            this.AuctionFeedbackPointComboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.AuctionFeedbackPointComboBox.Location = new System.Drawing.Point(3, 16);
            this.AuctionFeedbackPointComboBox.Name = "AuctionFeedbackPointComboBox";
            this.AuctionFeedbackPointComboBox.Size = new System.Drawing.Size(121, 21);
            this.AuctionFeedbackPointComboBox.TabIndex = 6;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 40);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(97, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Értékelés szövege:";
            // 
            // AuctionFeedbackDescriptionTextBox
            // 
            this.AuctionFeedbackDescriptionTextBox.Location = new System.Drawing.Point(3, 56);
            this.AuctionFeedbackDescriptionTextBox.Multiline = true;
            this.AuctionFeedbackDescriptionTextBox.Name = "AuctionFeedbackDescriptionTextBox";
            this.AuctionFeedbackDescriptionTextBox.Size = new System.Drawing.Size(192, 35);
            this.AuctionFeedbackDescriptionTextBox.TabIndex = 3;
            // 
            // AuctionSendFeedbackButton
            // 
            this.AuctionSendFeedbackButton.Location = new System.Drawing.Point(3, 97);
            this.AuctionSendFeedbackButton.Name = "AuctionSendFeedbackButton";
            this.AuctionSendFeedbackButton.Size = new System.Drawing.Size(75, 23);
            this.AuctionSendFeedbackButton.TabIndex = 4;
            this.AuctionSendFeedbackButton.Text = "Elküldés";
            this.AuctionSendFeedbackButton.UseVisualStyleBackColor = true;
            this.AuctionSendFeedbackButton.Click += new System.EventHandler(this.AuctionSendFeedbackButton_Click);
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.Controls.Add(this.flowLayoutPanel15);
            this.flowLayoutPanel13.Controls.Add(this.AuctionWinnerFlowLayoutPanel);
            this.flowLayoutPanel13.Controls.Add(this.flowLayoutPanel17);
            this.flowLayoutPanel13.Location = new System.Drawing.Point(209, 3);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(335, 268);
            this.flowLayoutPanel13.TabIndex = 2;
            this.flowLayoutPanel13.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panels_MouseDown);
            // 
            // flowLayoutPanel15
            // 
            this.flowLayoutPanel15.Controls.Add(this.AuctionLicit1FlowLayoutPanel);
            this.flowLayoutPanel15.Controls.Add(this.flowLayoutPanel21);
            this.flowLayoutPanel15.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel15.Name = "flowLayoutPanel15";
            this.flowLayoutPanel15.Size = new System.Drawing.Size(332, 99);
            this.flowLayoutPanel15.TabIndex = 0;
            // 
            // AuctionLicit1FlowLayoutPanel
            // 
            this.AuctionLicit1FlowLayoutPanel.Controls.Add(this.flowLayoutPanel10);
            this.AuctionLicit1FlowLayoutPanel.Controls.Add(this.AuctionLicit2FlowLayoutPanel);
            this.AuctionLicit1FlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.AuctionLicit1FlowLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.AuctionLicit1FlowLayoutPanel.Name = "AuctionLicit1FlowLayoutPanel";
            this.AuctionLicit1FlowLayoutPanel.Size = new System.Drawing.Size(161, 93);
            this.AuctionLicit1FlowLayoutPanel.TabIndex = 0;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.Controls.Add(this.label13);
            this.flowLayoutPanel10.Controls.Add(this.AuctionPriceNowTextBox);
            this.flowLayoutPanel10.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(153, 39);
            this.flowLayoutPanel10.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Jelenlegi ár:";
            // 
            // AuctionPriceNowTextBox
            // 
            this.AuctionPriceNowTextBox.Location = new System.Drawing.Point(3, 16);
            this.AuctionPriceNowTextBox.Name = "AuctionPriceNowTextBox";
            this.AuctionPriceNowTextBox.Size = new System.Drawing.Size(100, 20);
            this.AuctionPriceNowTextBox.TabIndex = 2;
            // 
            // AuctionLicit2FlowLayoutPanel
            // 
            this.AuctionLicit2FlowLayoutPanel.Controls.Add(this.flowLayoutPanel1);
            this.AuctionLicit2FlowLayoutPanel.Controls.Add(this.AuctionMaxBidFlowLayoutPanel);
            this.AuctionLicit2FlowLayoutPanel.Location = new System.Drawing.Point(3, 48);
            this.AuctionLicit2FlowLayoutPanel.Name = "AuctionLicit2FlowLayoutPanel";
            this.AuctionLicit2FlowLayoutPanel.Size = new System.Drawing.Size(155, 42);
            this.AuctionLicit2FlowLayoutPanel.TabIndex = 4;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.label15);
            this.flowLayoutPanel1.Controls.Add(this.AuctionBidTextBox);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(70, 37);
            this.flowLayoutPanel1.TabIndex = 4;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Licit:";
            // 
            // AuctionBidTextBox
            // 
            this.AuctionBidTextBox.Location = new System.Drawing.Point(3, 16);
            this.AuctionBidTextBox.Name = "AuctionBidTextBox";
            this.AuctionBidTextBox.Size = new System.Drawing.Size(64, 20);
            this.AuctionBidTextBox.TabIndex = 3;
            // 
            // AuctionMaxBidFlowLayoutPanel
            // 
            this.AuctionMaxBidFlowLayoutPanel.Controls.Add(this.label1);
            this.AuctionMaxBidFlowLayoutPanel.Controls.Add(this.AuctionMAxBidTextBox);
            this.AuctionMaxBidFlowLayoutPanel.Location = new System.Drawing.Point(79, 3);
            this.AuctionMaxBidFlowLayoutPanel.Name = "AuctionMaxBidFlowLayoutPanel";
            this.AuctionMaxBidFlowLayoutPanel.Size = new System.Drawing.Size(70, 37);
            this.AuctionMaxBidFlowLayoutPanel.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Max licitem:";
            // 
            // AuctionMAxBidTextBox
            // 
            this.AuctionMAxBidTextBox.Enabled = false;
            this.AuctionMAxBidTextBox.Location = new System.Drawing.Point(3, 16);
            this.AuctionMAxBidTextBox.Name = "AuctionMAxBidTextBox";
            this.AuctionMAxBidTextBox.Size = new System.Drawing.Size(64, 20);
            this.AuctionMAxBidTextBox.TabIndex = 3;
            // 
            // flowLayoutPanel21
            // 
            this.flowLayoutPanel21.Controls.Add(this.label14);
            this.flowLayoutPanel21.Controls.Add(this.AuctionMinPriceTextBox);
            this.flowLayoutPanel21.Controls.Add(this.PlaceholderLabel);
            this.flowLayoutPanel21.Controls.Add(this.AuctionBidButton);
            this.flowLayoutPanel21.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel21.Location = new System.Drawing.Point(170, 3);
            this.flowLayoutPanel21.Name = "flowLayoutPanel21";
            this.flowLayoutPanel21.Size = new System.Drawing.Size(159, 92);
            this.flowLayoutPanel21.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Minimális ár:";
            // 
            // AuctionMinPriceTextBox
            // 
            this.AuctionMinPriceTextBox.Location = new System.Drawing.Point(3, 16);
            this.AuctionMinPriceTextBox.Name = "AuctionMinPriceTextBox";
            this.AuctionMinPriceTextBox.Size = new System.Drawing.Size(100, 20);
            this.AuctionMinPriceTextBox.TabIndex = 1;
            // 
            // PlaceholderLabel
            // 
            this.PlaceholderLabel.AutoSize = true;
            this.PlaceholderLabel.Location = new System.Drawing.Point(3, 39);
            this.PlaceholderLabel.Name = "PlaceholderLabel";
            this.PlaceholderLabel.Size = new System.Drawing.Size(0, 13);
            this.PlaceholderLabel.TabIndex = 3;
            // 
            // AuctionBidButton
            // 
            this.AuctionBidButton.Location = new System.Drawing.Point(3, 55);
            this.AuctionBidButton.Name = "AuctionBidButton";
            this.AuctionBidButton.Size = new System.Drawing.Size(98, 23);
            this.AuctionBidButton.TabIndex = 2;
            this.AuctionBidButton.Text = "Licitálás";
            this.AuctionBidButton.UseVisualStyleBackColor = true;
            this.AuctionBidButton.Click += new System.EventHandler(this.AuctionBidButton_Click);
            // 
            // AuctionWinnerFlowLayoutPanel
            // 
            this.AuctionWinnerFlowLayoutPanel.Controls.Add(this.flowLayoutPanel2);
            this.AuctionWinnerFlowLayoutPanel.Controls.Add(this.EmailFlowLayoutPanel);
            this.AuctionWinnerFlowLayoutPanel.Location = new System.Drawing.Point(3, 108);
            this.AuctionWinnerFlowLayoutPanel.Name = "AuctionWinnerFlowLayoutPanel";
            this.AuctionWinnerFlowLayoutPanel.Size = new System.Drawing.Size(332, 44);
            this.AuctionWinnerFlowLayoutPanel.TabIndex = 1;
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.Controls.Add(this.AuctionStartDateFlowLayoutPanel);
            this.flowLayoutPanel17.Controls.Add(this.flowLayoutPanel19);
            this.flowLayoutPanel17.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel17.Location = new System.Drawing.Point(3, 158);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(332, 102);
            this.flowLayoutPanel17.TabIndex = 2;
            // 
            // AuctionStartDateFlowLayoutPanel
            // 
            this.AuctionStartDateFlowLayoutPanel.Controls.Add(this.label10);
            this.AuctionStartDateFlowLayoutPanel.Controls.Add(this.AuctionStartDateTimePicker);
            this.AuctionStartDateFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.AuctionStartDateFlowLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.AuctionStartDateFlowLayoutPanel.Name = "AuctionStartDateFlowLayoutPanel";
            this.AuctionStartDateFlowLayoutPanel.Size = new System.Drawing.Size(328, 43);
            this.AuctionStartDateFlowLayoutPanel.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Aukció kezdete:";
            // 
            // AuctionStartDateTimePicker
            // 
            this.AuctionStartDateTimePicker.Location = new System.Drawing.Point(3, 16);
            this.AuctionStartDateTimePicker.Name = "AuctionStartDateTimePicker";
            this.AuctionStartDateTimePicker.Size = new System.Drawing.Size(321, 20);
            this.AuctionStartDateTimePicker.TabIndex = 1;
            // 
            // flowLayoutPanel19
            // 
            this.flowLayoutPanel19.Controls.Add(this.label11);
            this.flowLayoutPanel19.Controls.Add(this.AuctionEndDateTimePicker);
            this.flowLayoutPanel19.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel19.Location = new System.Drawing.Point(3, 52);
            this.flowLayoutPanel19.Name = "flowLayoutPanel19";
            this.flowLayoutPanel19.Size = new System.Drawing.Size(327, 43);
            this.flowLayoutPanel19.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Aukció vége:";
            // 
            // AuctionEndDateTimePicker
            // 
            this.AuctionEndDateTimePicker.Location = new System.Drawing.Point(3, 16);
            this.AuctionEndDateTimePicker.Name = "AuctionEndDateTimePicker";
            this.AuctionEndDateTimePicker.Size = new System.Drawing.Size(321, 20);
            this.AuctionEndDateTimePicker.TabIndex = 2;
            // 
            // flowLayoutPanel14
            // 
            this.flowLayoutPanel14.Controls.Add(this.AuctionPictureBox);
            this.flowLayoutPanel14.Controls.Add(this.AuctionGetPictureFlowLayoutPanel);
            this.flowLayoutPanel14.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel14.Location = new System.Drawing.Point(550, 3);
            this.flowLayoutPanel14.Name = "flowLayoutPanel14";
            this.flowLayoutPanel14.Size = new System.Drawing.Size(260, 296);
            this.flowLayoutPanel14.TabIndex = 3;
            // 
            // AuctionPictureBox
            // 
            this.AuctionPictureBox.Location = new System.Drawing.Point(3, 3);
            this.AuctionPictureBox.Name = "AuctionPictureBox";
            this.AuctionPictureBox.Size = new System.Drawing.Size(255, 255);
            this.AuctionPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AuctionPictureBox.TabIndex = 4;
            this.AuctionPictureBox.TabStop = false;
            this.AuctionPictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panels_MouseDown);
            // 
            // AuctionGetPictureFlowLayoutPanel
            // 
            this.AuctionGetPictureFlowLayoutPanel.Controls.Add(this.AuctionBrowsePictureButton);
            this.AuctionGetPictureFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.AuctionGetPictureFlowLayoutPanel.Location = new System.Drawing.Point(3, 264);
            this.AuctionGetPictureFlowLayoutPanel.Name = "AuctionGetPictureFlowLayoutPanel";
            this.AuctionGetPictureFlowLayoutPanel.Size = new System.Drawing.Size(253, 28);
            this.AuctionGetPictureFlowLayoutPanel.TabIndex = 23;
            // 
            // AuctionBrowsePictureButton
            // 
            this.AuctionBrowsePictureButton.Location = new System.Drawing.Point(175, 3);
            this.AuctionBrowsePictureButton.Name = "AuctionBrowsePictureButton";
            this.AuctionBrowsePictureButton.Size = new System.Drawing.Size(75, 23);
            this.AuctionBrowsePictureButton.TabIndex = 22;
            this.AuctionBrowsePictureButton.Text = "Képtallózása";
            this.AuctionBrowsePictureButton.UseVisualStyleBackColor = true;
            this.AuctionBrowsePictureButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // AuctionDescriptionFlowLayoutPanel
            // 
            this.AuctionDescriptionFlowLayoutPanel.Controls.Add(this.label9);
            this.AuctionDescriptionFlowLayoutPanel.Controls.Add(this.AuctionDescriptionTextBox);
            this.AuctionDescriptionFlowLayoutPanel.Location = new System.Drawing.Point(3, 409);
            this.AuctionDescriptionFlowLayoutPanel.Name = "AuctionDescriptionFlowLayoutPanel";
            this.AuctionDescriptionFlowLayoutPanel.Size = new System.Drawing.Size(811, 100);
            this.AuctionDescriptionFlowLayoutPanel.TabIndex = 3;
            this.AuctionDescriptionFlowLayoutPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panels_MouseDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Leírás";
            // 
            // AuctionDescriptionTextBox
            // 
            this.AuctionDescriptionTextBox.Location = new System.Drawing.Point(46, 3);
            this.AuctionDescriptionTextBox.Multiline = true;
            this.AuctionDescriptionTextBox.Name = "AuctionDescriptionTextBox";
            this.AuctionDescriptionTextBox.Size = new System.Drawing.Size(760, 91);
            this.AuctionDescriptionTextBox.TabIndex = 3;
            // 
            // AuctionQuestionFlowLayoutPanel
            // 
            this.AuctionQuestionFlowLayoutPanel.Controls.Add(this.label16);
            this.AuctionQuestionFlowLayoutPanel.Controls.Add(this.AuctionQuestionListBox);
            this.AuctionQuestionFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.AuctionQuestionFlowLayoutPanel.Location = new System.Drawing.Point(3, 515);
            this.AuctionQuestionFlowLayoutPanel.Name = "AuctionQuestionFlowLayoutPanel";
            this.AuctionQuestionFlowLayoutPanel.Size = new System.Drawing.Size(811, 154);
            this.AuctionQuestionFlowLayoutPanel.TabIndex = 4;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(115, 13);
            this.label16.TabIndex = 4;
            this.label16.Text = "Kérdések és Válaszok:";
            // 
            // AuctionQuestionListBox
            // 
            this.AuctionQuestionListBox.BackColor = System.Drawing.SystemColors.Control;
            this.AuctionQuestionListBox.FormattingEnabled = true;
            this.AuctionQuestionListBox.Location = new System.Drawing.Point(3, 16);
            this.AuctionQuestionListBox.Name = "AuctionQuestionListBox";
            this.AuctionQuestionListBox.Size = new System.Drawing.Size(803, 134);
            this.AuctionQuestionListBox.TabIndex = 5;
            this.AuctionQuestionListBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.AuctionQuestionListBox_MouseDoubleClick);
            // 
            // flowLayoutPanel26
            // 
            this.flowLayoutPanel26.Controls.Add(this.flowLayoutPanel4);
            this.flowLayoutPanel26.Controls.Add(this.flowLayoutPanel5);
            this.flowLayoutPanel26.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel26.Location = new System.Drawing.Point(3, 675);
            this.flowLayoutPanel26.Name = "flowLayoutPanel26";
            this.flowLayoutPanel26.Size = new System.Drawing.Size(811, 30);
            this.flowLayoutPanel26.TabIndex = 6;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.AuctionSaveButton);
            this.flowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(374, 3);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(434, 27);
            this.flowLayoutPanel4.TabIndex = 1;
            // 
            // AuctionSaveButton
            // 
            this.AuctionSaveButton.Location = new System.Drawing.Point(353, 3);
            this.AuctionSaveButton.Name = "AuctionSaveButton";
            this.AuctionSaveButton.Size = new System.Drawing.Size(78, 23);
            this.AuctionSaveButton.TabIndex = 0;
            this.AuctionSaveButton.Text = "Létrehozás";
            this.AuctionSaveButton.UseVisualStyleBackColor = true;
            this.AuctionSaveButton.Click += new System.EventHandler(this.AuctionSaveButton_Click);
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(365, 27);
            this.flowLayoutPanel5.TabIndex = 2;
            // 
            // flowLayoutPanel24
            // 
            this.flowLayoutPanel24.Location = new System.Drawing.Point(823, 3);
            this.flowLayoutPanel24.Name = "flowLayoutPanel24";
            this.flowLayoutPanel24.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel24.TabIndex = 5;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "\"kép|*.jpg;*.png;*.bmp\"";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::AukcioKliens.Properties.Resources.close;
            this.pictureBox1.Location = new System.Drawing.Point(803, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(20, 20);
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.pictureBox1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::AukcioKliens.Properties.Resources.min;
            this.pictureBox2.Location = new System.Drawing.Point(777, 1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(20, 20);
            this.pictureBox2.TabIndex = 23;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            this.pictureBox2.MouseEnter += new System.EventHandler(this.pictureBox2_MouseEnter);
            this.pictureBox2.MouseLeave += new System.EventHandler(this.pictureBox2_MouseLeave);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(826, 24);
            this.panel1.TabIndex = 24;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panels_MouseDown);
            // 
            // EmailFlowLayoutPanel
            // 
            this.EmailFlowLayoutPanel.Controls.Add(this.EmailLabel);
            this.EmailFlowLayoutPanel.Controls.Add(this.EmailTextBox);
            this.EmailFlowLayoutPanel.Location = new System.Drawing.Point(167, 3);
            this.EmailFlowLayoutPanel.Name = "EmailFlowLayoutPanel";
            this.EmailFlowLayoutPanel.Size = new System.Drawing.Size(162, 41);
            this.EmailFlowLayoutPanel.TabIndex = 33;
            // 
            // EmailTextBox
            // 
            this.EmailTextBox.Location = new System.Drawing.Point(3, 16);
            this.EmailTextBox.Name = "EmailTextBox";
            this.EmailTextBox.Size = new System.Drawing.Size(153, 20);
            this.EmailTextBox.TabIndex = 30;
            // 
            // EmailLabel
            // 
            this.EmailLabel.AutoSize = true;
            this.EmailLabel.Location = new System.Drawing.Point(3, 0);
            this.EmailLabel.Name = "EmailLabel";
            this.EmailLabel.Size = new System.Drawing.Size(91, 13);
            this.EmailLabel.TabIndex = 31;
            this.EmailLabel.Text = "Eladó email címe:";
            // 
            // AuctionBidOwnerTextBox
            // 
            this.AuctionBidOwnerTextBox.Location = new System.Drawing.Point(3, 16);
            this.AuctionBidOwnerTextBox.Name = "AuctionBidOwnerTextBox";
            this.AuctionBidOwnerTextBox.Size = new System.Drawing.Size(153, 20);
            this.AuctionBidOwnerTextBox.TabIndex = 30;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 13);
            this.label12.TabIndex = 31;
            this.label12.Text = "Jelenlegi nyertes:";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.label12);
            this.flowLayoutPanel2.Controls.Add(this.AuctionBidOwnerTextBox);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(158, 41);
            this.flowLayoutPanel2.TabIndex = 32;
            // 
            // AuctionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 750);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.AuctionFlowLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AuctionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Aukcio";
            this.Load += new System.EventHandler(this.auctionForm_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panels_MouseDown);
            this.AuctionFlowLayoutPanel.ResumeLayout(false);
            this.flowLayoutPanel9.ResumeLayout(false);
            this.flowLayoutPanel9.PerformLayout();
            this.flowLayoutPanel11.ResumeLayout(false);
            this.AuctionFeedbackFlowLayoutPanel.ResumeLayout(false);
            this.flowLayoutPanel22.ResumeLayout(false);
            this.flowLayoutPanel22.PerformLayout();
            this.flowLayoutPanel13.ResumeLayout(false);
            this.flowLayoutPanel15.ResumeLayout(false);
            this.AuctionLicit1FlowLayoutPanel.ResumeLayout(false);
            this.flowLayoutPanel10.ResumeLayout(false);
            this.flowLayoutPanel10.PerformLayout();
            this.AuctionLicit2FlowLayoutPanel.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.AuctionMaxBidFlowLayoutPanel.ResumeLayout(false);
            this.AuctionMaxBidFlowLayoutPanel.PerformLayout();
            this.flowLayoutPanel21.ResumeLayout(false);
            this.flowLayoutPanel21.PerformLayout();
            this.AuctionWinnerFlowLayoutPanel.ResumeLayout(false);
            this.flowLayoutPanel17.ResumeLayout(false);
            this.AuctionStartDateFlowLayoutPanel.ResumeLayout(false);
            this.AuctionStartDateFlowLayoutPanel.PerformLayout();
            this.flowLayoutPanel19.ResumeLayout(false);
            this.flowLayoutPanel19.PerformLayout();
            this.flowLayoutPanel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AuctionPictureBox)).EndInit();
            this.AuctionGetPictureFlowLayoutPanel.ResumeLayout(false);
            this.AuctionDescriptionFlowLayoutPanel.ResumeLayout(false);
            this.AuctionDescriptionFlowLayoutPanel.PerformLayout();
            this.AuctionQuestionFlowLayoutPanel.ResumeLayout(false);
            this.AuctionQuestionFlowLayoutPanel.PerformLayout();
            this.flowLayoutPanel26.ResumeLayout(false);
            this.flowLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.EmailFlowLayoutPanel.ResumeLayout(false);
            this.EmailFlowLayoutPanel.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel AuctionFlowLayoutPanel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox AuctionNameTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox AuctionCategoryTextBox;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.FlowLayoutPanel AuctionFeedbackFlowLayoutPanel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel22;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox AuctionFeedbackPointComboBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox AuctionFeedbackDescriptionTextBox;
        private System.Windows.Forms.Button AuctionSendFeedbackButton;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        private System.Windows.Forms.FlowLayoutPanel AuctionLicit1FlowLayoutPanel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox AuctionPriceNowTextBox;
        private System.Windows.Forms.FlowLayoutPanel AuctionLicit2FlowLayoutPanel;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox AuctionBidTextBox;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel21;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox AuctionMinPriceTextBox;
        private System.Windows.Forms.Label PlaceholderLabel;
        private System.Windows.Forms.Button AuctionBidButton;
        private System.Windows.Forms.FlowLayoutPanel AuctionWinnerFlowLayoutPanel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private System.Windows.Forms.FlowLayoutPanel AuctionStartDateFlowLayoutPanel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker AuctionStartDateTimePicker;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel19;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker AuctionEndDateTimePicker;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
        private System.Windows.Forms.PictureBox AuctionPictureBox;
        private System.Windows.Forms.FlowLayoutPanel AuctionGetPictureFlowLayoutPanel;
        private System.Windows.Forms.Button AuctionBrowsePictureButton;
        private System.Windows.Forms.FlowLayoutPanel AuctionDescriptionFlowLayoutPanel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox AuctionDescriptionTextBox;
        private System.Windows.Forms.FlowLayoutPanel AuctionQuestionFlowLayoutPanel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ListBox AuctionQuestionListBox;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel26;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button AuctionSaveButton;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel24;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.FlowLayoutPanel AuctionMaxBidFlowLayoutPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox AuctionMAxBidTextBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox AuctionBidOwnerTextBox;
        private System.Windows.Forms.FlowLayoutPanel EmailFlowLayoutPanel;
        private System.Windows.Forms.Label EmailLabel;
        private System.Windows.Forms.TextBox EmailTextBox;
    }
}