﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AukcioKliens
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
            registerButton.Enabled=false;

        }
        private void contentChange(object sender, EventArgs e)
        {
            if (userNameTextBox.TextLength > 0 && passwordTextBox.TextLength > 0 && emailTextBox.TextLength > 0
                && nameTextBox.TextLength > 0 && phoneTextbox.TextLength > 0 && addressTextBox.TextLength > 0)
            {
                registerButton.Enabled = true;
            }
            else
            {
                registerButton.Enabled = false;
            }
        }
        private void registerButton_Click(object sender, EventArgs e)
        {
            
                var contetntJSON = (dynamic)new JsonObject();
                contetntJSON.Add("Username", userNameTextBox.Text);
                contetntJSON.Add("Password", passwordTextBox.Text);
                contetntJSON.Add("Email", emailTextBox.Text);
                contetntJSON.Add("Name", nameTextBox.Text);
                contetntJSON.Add("Phone", phoneTextbox.Text);
                contetntJSON.Add("Address", addressTextBox.Text);

                var content = new StringContent(contetntJSON.ToString(), Encoding.UTF8, "application/json");
            try
            {
                var result = WebClient.Client.PutAsync("register", content).Result;
                if (result.IsSuccessStatusCode)
                {
                        MessageBox.Show("Sikeres regisztráció!");
                        this.Close();
                }
                else
                {
                    MessageBox.Show("Ez a felhasználó név már foglalt!");
                }
            }
            catch (System.AggregateException) 
            {
                MessageBox.Show("Unknown error!");
            }
            
        }
        private bool isValidEmailAddress(string email) 
        {
            if (email.Length == 0)
            {
                return false;
            }

            // Confirm that there is an "@" and a "." in the email address, and in the correct order.
            if (email.IndexOf("@") > -1)
            {
                if (email.IndexOf(".", email.IndexOf("@")) > email.IndexOf("@"))
                {
                    return true;
                }
            }
            return false;
        }

        private void emailTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (!isValidEmailAddress(emailTextBox.Text)) 
            {
                emailTextBox.BackColor = Color.Beige;
                emailTextBox.ForeColor = Color.Red;
                emailTextBox.Select(0, emailTextBox.Text.Length);
                errorProvider1.SetError(emailTextBox, "Rossz E-mail cím formátum!");

            }
        }

        private void emailTextBox_Validated(object sender, EventArgs e)
        {
           // errorProvider1.SetError(emailTextBox, "");
        }
    }
}
