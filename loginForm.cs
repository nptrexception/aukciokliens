﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Json;
using System.Runtime.InteropServices;

namespace AukcioKliens
{

    public partial class LoginForm : Form
    {

        public LoginForm()
        {
            InitializeComponent();
        }
        [DllImport("user32.dll", EntryPoint = "ReleaseCapture")]
        private static extern void ReleaseCapture();
        [DllImport("user32.dll", EntryPoint = "SendMessage")]
        private static extern void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        private void loginButton_Click(object sender, EventArgs e)
        {
            var client = WebClient.Client;
            var contentJSON = (dynamic)new JsonObject();
            contentJSON.Add("Username", userNameTextBox.Text);
            contentJSON.Add("Password", passwordTextBox.Text);
            var X = contentJSON.ToString();
            var content = new StringContent(contentJSON.ToString(), Encoding.UTF8, "application/json");
            try
            {
                var result = client.PostAsync("login", content).Result;

                switch (result.StatusCode)
                {
                    case HttpStatusCode.OK:
                        var contentString = result.Content.ReadAsStringAsync().Result;
                        var json = JsonObject.Parse(contentString);
                        if (json["Success"]==true)
                        {
                            WebClient.token = json["JwtToken"];
                            WebClient.currentUserID = json["UserId"];
                            MainForm mf = new MainForm();
                            this.Hide();
                            mf.Show();
                        }
                        else
                        {
                            if (json["Error"]!=null)
                                MessageBox.Show(json["Error"]);
                        }
                        //WebClient.currentUserID = json["id"];
                        break;    
                    case HttpStatusCode.InternalServerError:
                    loginErrorLabel.Text = "SZERVER HIBA!";
                        break;
                    case HttpStatusCode.NotFound:
                    loginErrorLabel.Text = "NOT FOUND!";
                        break;
                    case HttpStatusCode.Unauthorized:
                    loginErrorLabel.Text = "TÉVES FELHASZNÁLÓNÉV VAGY JELSZÓ!";
                        break;
                    default:
                        MessageBox.Show("Unknown error!");
                        break;
                }
            }
            catch (System.AggregateException) 
            {
                MessageBox.Show("Unknown error!");
            }
             
            /*
            WebClient.token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOjIsInRva2VuSWQiOiI3NjZmMzhmNi1iMDViLTRiMmMtOWZmMS00NzcyMzJjOGVhZWQiLCJleHAiOjE2ODE4NDUyMDcuMH0.oIZNyipv8swAeXJxnZEKUQ5JQlaoziiyOeZX4ugelnQ";
            WebClient.currentUserID = 2;
            MainForm mf = new MainForm();
            this.Hide();
            mf.Show();
            */
        }

        private void registerButton_Click(object sender, EventArgs e)
        {
            loginErrorLabel.Text = "";
            registrationPanel.Visible = true;
            loginPanel.Visible = false;
            usernameRegtextBox.Text = "FELHASZNÁLÓNÉV";
            usernameRegtextBox.ForeColor = Color.DimGray;
            emailRegtextBox.Text = "EMAIL";
            emailRegtextBox.ForeColor = Color.DimGray;
            pwd1RegtextBox.Text = "JELSZÓ";
            pwd1RegtextBox.ForeColor = Color.DimGray;
            pwd2RegtextBox.Text = "JELSZÓ ÚJRA";
            pwd2RegtextBox.ForeColor = Color.DimGray;
            RegisterForm rf = new RegisterForm();
            rf.Show();
        }

        private void loginTextBoxes_TextChanged(object sender, EventArgs e)
        {
            if (passwordTextBox.TextLength > 0 && userNameTextBox.TextLength > 0 && passwordTextBox.Text != "JELSZÓ" && userNameTextBox.Text != "FELHASZNÁLÓNÉV")
            {
                loginButton.Enabled = true;
            }
            else
            {
                loginButton.Enabled = false;
            }
        }

        private void userNameTextBox_Enter(object sender, EventArgs e)
        {
            loginErrorLabel.Text = "";
            if (userNameTextBox.Text == "FELHASZNÁLÓNÉV")
            {
                userNameTextBox.Text = "";
                userNameTextBox.ForeColor = Color.LightGray;
            }
        }

        private void userNameTextBox_Leave(object sender, EventArgs e)
        {
            if (userNameTextBox.Text == "")
            {
                userNameTextBox.Text = "FELHASZNÁLÓNÉV";
                userNameTextBox.ForeColor = Color.DimGray;
            }
        }

        private void passwordTextBox_Enter(object sender, EventArgs e)
        {
            loginErrorLabel.Text = "";
            if (passwordTextBox.Text == "JELSZÓ")
            {
                passwordTextBox.Text = "";
                passwordTextBox.ForeColor = Color.LightGray;
                passwordTextBox.UseSystemPasswordChar = true;
            }
        }

        private void passwordTextBox_Leave(object sender, EventArgs e)
        {
            if (passwordTextBox.Text == "")
            {
                passwordTextBox.Text = "JELSZÓ";
                passwordTextBox.ForeColor = Color.DimGray;
                passwordTextBox.UseSystemPasswordChar = false;
            }
        }

        private void closeBTN_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void minimalizeBTN_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {

        }

        private void movingObject_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void closeBTN2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void minimalizeBTN2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            registrationErrorLabel.Text = "";
            loginPanel.Visible = true;
            passwordTextBox.Text = "JELSZÓ";
            passwordTextBox.ForeColor = Color.DimGray;
            userNameTextBox.Text = "FELHASZNÁLÓNÉV";
            userNameTextBox.ForeColor = Color.DimGray;
            registrationPanel.Visible = false;
        }

        private void registrationButton_Click(object sender, EventArgs e)
        {
            if (pwd1RegtextBox.Text==pwd2RegtextBox.Text)
            {
                var contetntJSON = (dynamic)new JsonObject();
                contetntJSON.Add("Username", usernameRegtextBox.Text);
                contetntJSON.Add("Password", pwd1RegtextBox.Text);
                contetntJSON.Add("Email", emailRegtextBox.Text);

                var content = new StringContent(contetntJSON.ToString(), Encoding.UTF8, "application/json");
                var result = WebClient.Client.PutAsync("login", content).Result;
                if (result.IsSuccessStatusCode)
                {
                    var contentString = result.Content.ReadAsStringAsync().Result;
                    var json = JsonObject.Parse(contentString);

                    if (json["success"] == false)
                    {
                        registrationErrorLabel.Text = "EZ A FELHASZNÁLÓ NÉV MÁR FOGLALT!";
                    }
                    else
                    {
                        registrationErrorLabel.Text = "SIKERES REGISZTRÁCIÓ!";
                        this.Close();
                    }
                }
            }
            else
            {
                registrationErrorLabel.Text = "A KETTŐ JELSZÓ MEZŐ NEM EGYEZIK MEG!";
            }
        }

        private void contentChange(object sender, EventArgs e)
        {
            if (usernameRegtextBox.TextLength > 0 && emailRegtextBox.TextLength > 0 && pwd1RegtextBox.TextLength > 0 && pwd2RegtextBox.TextLength > 0 &&
                usernameRegtextBox.Text!="FELHASZNÁLÓNÉV" && emailRegtextBox.Text!="EMAIL" && pwd1RegtextBox.Text!="JELSZÓ" && pwd2RegtextBox.Text!="JELSZÓ ÚJRA")
            {
                registrationButton.Enabled = true;
            }
            else
            {
                registrationButton.Enabled = false;
            }
        }

        private void usernameRegtextBox_Enter(object sender, EventArgs e)
        {
            registrationErrorLabel.Text = "";
            if (usernameRegtextBox.Text == "FELHASZNÁLÓNÉV")
            {
                usernameRegtextBox.Text = "";
                usernameRegtextBox.ForeColor = Color.LightGray;
            }
        }

        private void usernameRegtextBox_Leave(object sender, EventArgs e)
        {
            if (usernameRegtextBox.Text == "")
            {
                usernameRegtextBox.Text = "FELHASZNÁLÓNÉV";
                usernameRegtextBox.ForeColor = Color.DimGray;
            }
        }

        private void emailRegtextBox_Enter(object sender, EventArgs e)
        {
            registrationErrorLabel.Text = "";
            if (emailRegtextBox.Text == "EMAIL")
            {
                emailRegtextBox.Text = "";
                emailRegtextBox.ForeColor = Color.LightGray;
            }
        }

        private void emailRegtextBox_Leave(object sender, EventArgs e)
        {
            if (emailRegtextBox.Text == "")
            {
                emailRegtextBox.Text = "EMAIL";
                emailRegtextBox.ForeColor = Color.DimGray;
            }
        }

        private void pwd1RegtextBox_Enter(object sender, EventArgs e)
        {
            registrationErrorLabel.Text = "";
            if (pwd1RegtextBox.Text == "JELSZÓ")
            {
                pwd1RegtextBox.Text = "";
                pwd1RegtextBox.ForeColor = Color.LightGray;
            }
        }

        private void pwd1RegtextBox_Leave(object sender, EventArgs e)
        {
            if (pwd1RegtextBox.Text == "")
            {
                pwd1RegtextBox.Text = "JELSZÓ";
                pwd1RegtextBox.ForeColor = Color.DimGray;
            }
        }

        private void pwd2RegtextBox_Enter(object sender, EventArgs e)
        {
            registrationErrorLabel.Text = "";
            if (pwd2RegtextBox.Text == "JELSZÓ ÚJRA")
            {
                pwd2RegtextBox.Text = "";
                pwd2RegtextBox.ForeColor = Color.LightGray;
            }
        }

        private void pwd2RegtextBox_Leave(object sender, EventArgs e)
        {
            if (pwd2RegtextBox.Text == "")
            {
                pwd2RegtextBox.Text = "JELSZÓ ÚJRA";
                pwd2RegtextBox.ForeColor = Color.DimGray;
            }
        }

        private void closeBTN_MouseEnter(object sender, EventArgs e)
        {
            closeBTN.BackColor = Color.DarkRed;
        }

        private void minimalizeBTN_MouseEnter(object sender, EventArgs e)
        {
            minimalizeBTN.BackColor = Color.DarkRed;
        }

        private void closeBTN_MouseLeave(object sender, EventArgs e)
        {
            closeBTN.BackColor = closeBTN.Parent.BackColor;
        }

        private void minimalizeBTN_MouseLeave(object sender, EventArgs e)
        {
            minimalizeBTN.BackColor = minimalizeBTN.Parent.BackColor;
        }
    }  
}
