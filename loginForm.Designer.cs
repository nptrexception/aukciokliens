﻿namespace AukcioKliens
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userNameTextBox = new System.Windows.Forms.TextBox();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.registerButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.closeBTN = new System.Windows.Forms.PictureBox();
            this.minimalizeBTN = new System.Windows.Forms.PictureBox();
            this.separator1 = new Controls.Separator();
            this.separator2 = new Controls.Separator();
            this.loginButton = new System.Windows.Forms.Button();
            this.loginPanel = new System.Windows.Forms.Panel();
            this.loginErrorLabel = new System.Windows.Forms.Label();
            this.registrationPanel = new System.Windows.Forms.Panel();
            this.registrationErrorLabel = new System.Windows.Forms.Label();
            this.separator6 = new Controls.Separator();
            this.pwd2RegtextBox = new System.Windows.Forms.TextBox();
            this.separator5 = new Controls.Separator();
            this.pwd1RegtextBox = new System.Windows.Forms.TextBox();
            this.separator3 = new Controls.Separator();
            this.emailRegtextBox = new System.Windows.Forms.TextBox();
            this.separator4 = new Controls.Separator();
            this.usernameRegtextBox = new System.Windows.Forms.TextBox();
            this.registrationButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.minimalizeBTN2 = new System.Windows.Forms.PictureBox();
            this.closeBTN2 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.closeBTN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimalizeBTN)).BeginInit();
            this.loginPanel.SuspendLayout();
            this.registrationPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.minimalizeBTN2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeBTN2)).BeginInit();
            this.SuspendLayout();
            // 
            // userNameTextBox
            // 
            this.userNameTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.userNameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.userNameTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.userNameTextBox.ForeColor = System.Drawing.Color.DimGray;
            this.userNameTextBox.Location = new System.Drawing.Point(54, 72);
            this.userNameTextBox.MaxLength = 50;
            this.userNameTextBox.Name = "userNameTextBox";
            this.userNameTextBox.Size = new System.Drawing.Size(408, 20);
            this.userNameTextBox.TabIndex = 1;
            this.userNameTextBox.Text = "FELHASZNÁLÓNÉV";
            this.userNameTextBox.TextChanged += new System.EventHandler(this.loginTextBoxes_TextChanged);
            this.userNameTextBox.Enter += new System.EventHandler(this.userNameTextBox_Enter);
            this.userNameTextBox.Leave += new System.EventHandler(this.userNameTextBox_Leave);
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.passwordTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.passwordTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.passwordTextBox.ForeColor = System.Drawing.Color.DimGray;
            this.passwordTextBox.Location = new System.Drawing.Point(54, 126);
            this.passwordTextBox.MaxLength = 50;
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.Size = new System.Drawing.Size(408, 20);
            this.passwordTextBox.TabIndex = 2;
            this.passwordTextBox.Text = "JELSZÓ";
            this.passwordTextBox.TextChanged += new System.EventHandler(this.loginTextBoxes_TextChanged);
            this.passwordTextBox.Enter += new System.EventHandler(this.passwordTextBox_Enter);
            this.passwordTextBox.Leave += new System.EventHandler(this.passwordTextBox_Leave);
            // 
            // registerButton
            // 
            this.registerButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.registerButton.FlatAppearance.BorderSize = 0;
            this.registerButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.registerButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.registerButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.registerButton.ForeColor = System.Drawing.Color.LightGray;
            this.registerButton.Location = new System.Drawing.Point(54, 275);
            this.registerButton.Name = "registerButton";
            this.registerButton.Size = new System.Drawing.Size(408, 40);
            this.registerButton.TabIndex = 0;
            this.registerButton.Text = "REGISZTRÁCIÓ";
            this.registerButton.UseVisualStyleBackColor = false;
            this.registerButton.Click += new System.EventHandler(this.registerButton_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkRed;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 330);
            this.panel1.TabIndex = 6;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.movingObject_MouseDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Algerian", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Location = new System.Drawing.Point(54, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 54);
            this.label2.TabIndex = 0;
            this.label2.Text = "NPTR";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(168, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 33);
            this.label1.TabIndex = 8;
            this.label1.Text = "BEJELENTKEZÉS";
            // 
            // closeBTN
            // 
            this.closeBTN.Image = global::AukcioKliens.Properties.Resources.close;
            this.closeBTN.Location = new System.Drawing.Point(504, 3);
            this.closeBTN.Name = "closeBTN";
            this.closeBTN.Size = new System.Drawing.Size(20, 20);
            this.closeBTN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.closeBTN.TabIndex = 9;
            this.closeBTN.TabStop = false;
            this.closeBTN.Click += new System.EventHandler(this.closeBTN_Click);
            this.closeBTN.MouseEnter += new System.EventHandler(this.closeBTN_MouseEnter);
            this.closeBTN.MouseLeave += new System.EventHandler(this.closeBTN_MouseLeave);  
            // 
            // minimalizeBTN
            // 
            this.minimalizeBTN.Image = global::AukcioKliens.Properties.Resources.min;
            this.minimalizeBTN.Location = new System.Drawing.Point(478, 3);
            this.minimalizeBTN.Name = "minimalizeBTN";
            this.minimalizeBTN.Size = new System.Drawing.Size(20, 20);
            this.minimalizeBTN.TabIndex = 10;
            this.minimalizeBTN.TabStop = false;
            this.minimalizeBTN.Click += new System.EventHandler(this.minimalizeBTN_Click);
            this.minimalizeBTN.MouseEnter += new System.EventHandler(this.minimalizeBTN_MouseEnter);
            this.minimalizeBTN.MouseLeave += new System.EventHandler(this.minimalizeBTN_MouseLeave);
            // 
            // separator1
            // 
            this.separator1.BackColor = System.Drawing.Color.DimGray;
            this.separator1.Color = System.Drawing.Color.Empty;
            this.separator1.Direction = null;
            this.separator1.DisabledColor = System.Drawing.Color.Empty;
            this.separator1.Location = new System.Drawing.Point(54, 98);
            this.separator1.Name = "separator1";
            this.separator1.Size = new System.Drawing.Size(408, 2);
            this.separator1.TabIndex = 11;
            this.separator1.Text = "separator1";
            this.separator1.Thickness = 0;
            // 
            // separator2
            // 
            this.separator2.BackColor = System.Drawing.Color.DimGray;
            this.separator2.Color = System.Drawing.Color.Empty;
            this.separator2.Direction = null;
            this.separator2.DisabledColor = System.Drawing.Color.Empty;
            this.separator2.Location = new System.Drawing.Point(54, 152);
            this.separator2.Name = "separator2";
            this.separator2.Size = new System.Drawing.Size(408, 2);
            this.separator2.TabIndex = 12;
            this.separator2.Text = "separator2";
            this.separator2.Thickness = 0;
            // 
            // loginButton
            // 
            this.loginButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.loginButton.Enabled = false;
            this.loginButton.FlatAppearance.BorderSize = 0;
            this.loginButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.loginButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.loginButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loginButton.ForeColor = System.Drawing.Color.LightGray;
            this.loginButton.Location = new System.Drawing.Point(54, 229);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(408, 40);
            this.loginButton.TabIndex = 13;
            this.loginButton.Text = "BELÉPÉS";
            this.loginButton.UseVisualStyleBackColor = false;
            this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // loginPanel
            // 
            this.loginPanel.Controls.Add(this.loginErrorLabel);
            this.loginPanel.Controls.Add(this.separator2);
            this.loginPanel.Controls.Add(this.loginButton);
            this.loginPanel.Controls.Add(this.passwordTextBox);
            this.loginPanel.Controls.Add(this.registerButton);
            this.loginPanel.Controls.Add(this.label1);
            this.loginPanel.Controls.Add(this.minimalizeBTN);
            this.loginPanel.Controls.Add(this.separator1);
            this.loginPanel.Controls.Add(this.closeBTN);
            this.loginPanel.Controls.Add(this.userNameTextBox);
            this.loginPanel.Location = new System.Drawing.Point(253, 0);
            this.loginPanel.Name = "loginPanel";
            this.loginPanel.Size = new System.Drawing.Size(524, 318);
            this.loginPanel.TabIndex = 14;
            this.loginPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.movingObject_MouseDown);
            // 
            // loginErrorLabel
            // 
            this.loginErrorLabel.AutoSize = true;
            this.loginErrorLabel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.loginErrorLabel.ForeColor = System.Drawing.Color.White;
            this.loginErrorLabel.Location = new System.Drawing.Point(102, 174);
            this.loginErrorLabel.Name = "loginErrorLabel";
            this.loginErrorLabel.Size = new System.Drawing.Size(0, 21);
            this.loginErrorLabel.TabIndex = 14;
            // 
            // registrationPanel
            // 
            this.registrationPanel.Controls.Add(this.registrationErrorLabel);
            this.registrationPanel.Controls.Add(this.separator6);
            this.registrationPanel.Controls.Add(this.pwd2RegtextBox);
            this.registrationPanel.Controls.Add(this.separator5);
            this.registrationPanel.Controls.Add(this.pwd1RegtextBox);
            this.registrationPanel.Controls.Add(this.separator3);
            this.registrationPanel.Controls.Add(this.emailRegtextBox);
            this.registrationPanel.Controls.Add(this.separator4);
            this.registrationPanel.Controls.Add(this.usernameRegtextBox);
            this.registrationPanel.Controls.Add(this.registrationButton);
            this.registrationPanel.Controls.Add(this.backButton);
            this.registrationPanel.Controls.Add(this.label3);
            this.registrationPanel.Controls.Add(this.minimalizeBTN2);
            this.registrationPanel.Controls.Add(this.closeBTN2);
            this.registrationPanel.Location = new System.Drawing.Point(253, 0);
            this.registrationPanel.Name = "registrationPanel";
            this.registrationPanel.Size = new System.Drawing.Size(524, 330);
            this.registrationPanel.TabIndex = 14;
            this.registrationPanel.Visible = false;
            this.registrationPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.movingObject_MouseDown);
            // 
            // registrationErrorLabel
            // 
            this.registrationErrorLabel.AutoSize = true;
            this.registrationErrorLabel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.registrationErrorLabel.ForeColor = System.Drawing.Color.White;
            this.registrationErrorLabel.Location = new System.Drawing.Point(84, 197);
            this.registrationErrorLabel.Name = "registrationErrorLabel";
            this.registrationErrorLabel.Size = new System.Drawing.Size(0, 21);
            this.registrationErrorLabel.TabIndex = 24;
            // 
            // separator6
            // 
            this.separator6.BackColor = System.Drawing.Color.DimGray;
            this.separator6.Color = System.Drawing.Color.Empty;
            this.separator6.Direction = null;
            this.separator6.DisabledColor = System.Drawing.Color.Empty;
            this.separator6.Location = new System.Drawing.Point(54, 184);
            this.separator6.Name = "separator6";
            this.separator6.Size = new System.Drawing.Size(408, 2);
            this.separator6.TabIndex = 23;
            this.separator6.Text = "separator6";
            this.separator6.Thickness = 0;
            // 
            // pwd2RegtextBox
            // 
            this.pwd2RegtextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.pwd2RegtextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.pwd2RegtextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pwd2RegtextBox.ForeColor = System.Drawing.Color.DimGray;
            this.pwd2RegtextBox.Location = new System.Drawing.Point(54, 158);
            this.pwd2RegtextBox.MaxLength = 50;
            this.pwd2RegtextBox.Name = "pwd2RegtextBox";
            this.pwd2RegtextBox.Size = new System.Drawing.Size(408, 20);
            this.pwd2RegtextBox.TabIndex = 22;
            this.pwd2RegtextBox.Text = "JELSZÓ ÚJRA";
            this.pwd2RegtextBox.TextChanged += new System.EventHandler(this.contentChange);
            this.pwd2RegtextBox.Enter += new System.EventHandler(this.pwd2RegtextBox_Enter);
            this.pwd2RegtextBox.Leave += new System.EventHandler(this.pwd2RegtextBox_Leave);
            // 
            // separator5
            // 
            this.separator5.BackColor = System.Drawing.Color.DimGray;
            this.separator5.Color = System.Drawing.Color.Empty;
            this.separator5.Direction = null;
            this.separator5.DisabledColor = System.Drawing.Color.Empty;
            this.separator5.Location = new System.Drawing.Point(54, 150);
            this.separator5.Name = "separator5";
            this.separator5.Size = new System.Drawing.Size(408, 2);
            this.separator5.TabIndex = 21;
            this.separator5.Text = "separator5";
            this.separator5.Thickness = 0;
            // 
            // pwd1RegtextBox
            // 
            this.pwd1RegtextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.pwd1RegtextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.pwd1RegtextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pwd1RegtextBox.ForeColor = System.Drawing.Color.DimGray;
            this.pwd1RegtextBox.Location = new System.Drawing.Point(54, 124);
            this.pwd1RegtextBox.MaxLength = 50;
            this.pwd1RegtextBox.Name = "pwd1RegtextBox";
            this.pwd1RegtextBox.Size = new System.Drawing.Size(408, 20);
            this.pwd1RegtextBox.TabIndex = 20;
            this.pwd1RegtextBox.Text = "JELSZÓ";
            this.pwd1RegtextBox.TextChanged += new System.EventHandler(this.contentChange);
            this.pwd1RegtextBox.Enter += new System.EventHandler(this.pwd1RegtextBox_Enter);
            this.pwd1RegtextBox.Leave += new System.EventHandler(this.pwd1RegtextBox_Leave);
            // 
            // separator3
            // 
            this.separator3.BackColor = System.Drawing.Color.DimGray;
            this.separator3.Color = System.Drawing.Color.Empty;
            this.separator3.Direction = null;
            this.separator3.DisabledColor = System.Drawing.Color.Empty;
            this.separator3.Location = new System.Drawing.Point(54, 116);
            this.separator3.Name = "separator3";
            this.separator3.Size = new System.Drawing.Size(408, 2);
            this.separator3.TabIndex = 19;
            this.separator3.Text = "separator3";
            this.separator3.Thickness = 0;
            // 
            // emailRegtextBox
            // 
            this.emailRegtextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.emailRegtextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.emailRegtextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.emailRegtextBox.ForeColor = System.Drawing.Color.DimGray;
            this.emailRegtextBox.Location = new System.Drawing.Point(54, 90);
            this.emailRegtextBox.MaxLength = 50;
            this.emailRegtextBox.Name = "emailRegtextBox";
            this.emailRegtextBox.Size = new System.Drawing.Size(408, 20);
            this.emailRegtextBox.TabIndex = 17;
            this.emailRegtextBox.Text = "EMAIL";
            this.emailRegtextBox.TextChanged += new System.EventHandler(this.contentChange);
            this.emailRegtextBox.Enter += new System.EventHandler(this.emailRegtextBox_Enter);
            this.emailRegtextBox.Leave += new System.EventHandler(this.emailRegtextBox_Leave);
            // 
            // separator4
            // 
            this.separator4.BackColor = System.Drawing.Color.DimGray;
            this.separator4.Color = System.Drawing.Color.Empty;
            this.separator4.Direction = null;
            this.separator4.DisabledColor = System.Drawing.Color.Empty;
            this.separator4.Location = new System.Drawing.Point(54, 82);
            this.separator4.Name = "separator4";
            this.separator4.Size = new System.Drawing.Size(408, 2);
            this.separator4.TabIndex = 18;
            this.separator4.Text = "separator4";
            this.separator4.Thickness = 0;
            // 
            // usernameRegtextBox
            // 
            this.usernameRegtextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.usernameRegtextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.usernameRegtextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.usernameRegtextBox.ForeColor = System.Drawing.Color.DimGray;
            this.usernameRegtextBox.Location = new System.Drawing.Point(54, 56);
            this.usernameRegtextBox.MaxLength = 50;
            this.usernameRegtextBox.Name = "usernameRegtextBox";
            this.usernameRegtextBox.Size = new System.Drawing.Size(408, 20);
            this.usernameRegtextBox.TabIndex = 16;
            this.usernameRegtextBox.Text = "FELHASZNÁLÓNÉV";
            this.usernameRegtextBox.TextChanged += new System.EventHandler(this.contentChange);
            this.usernameRegtextBox.Enter += new System.EventHandler(this.usernameRegtextBox_Enter);
            this.usernameRegtextBox.Leave += new System.EventHandler(this.usernameRegtextBox_Leave);
            // 
            // registrationButton
            // 
            this.registrationButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.registrationButton.Enabled = false;
            this.registrationButton.FlatAppearance.BorderSize = 0;
            this.registrationButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.registrationButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.registrationButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.registrationButton.ForeColor = System.Drawing.Color.LightGray;
            this.registrationButton.Location = new System.Drawing.Point(54, 229);
            this.registrationButton.Name = "registrationButton";
            this.registrationButton.Size = new System.Drawing.Size(408, 40);
            this.registrationButton.TabIndex = 15;
            this.registrationButton.Text = "ELKÜLD";
            this.registrationButton.UseVisualStyleBackColor = false;
            this.registrationButton.Click += new System.EventHandler(this.registrationButton_Click);
            // 
            // backButton
            // 
            this.backButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.backButton.FlatAppearance.BorderSize = 0;
            this.backButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.backButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.backButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backButton.ForeColor = System.Drawing.Color.LightGray;
            this.backButton.Location = new System.Drawing.Point(54, 275);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(408, 40);
            this.backButton.TabIndex = 14;
            this.backButton.Text = "VISSZA";
            this.backButton.UseVisualStyleBackColor = false;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Location = new System.Drawing.Point(169, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(198, 33);
            this.label3.TabIndex = 13;
            this.label3.Text = "REGISZTRÁCIÓ";
            // 
            // minimalizeBTN2
            // 
            this.minimalizeBTN2.Image = global::AukcioKliens.Properties.Resources.min;
            this.minimalizeBTN2.Location = new System.Drawing.Point(478, 3);
            this.minimalizeBTN2.Name = "minimalizeBTN2";
            this.minimalizeBTN2.Size = new System.Drawing.Size(20, 20);
            this.minimalizeBTN2.TabIndex = 12;
            this.minimalizeBTN2.TabStop = false;
            this.minimalizeBTN2.Click += new System.EventHandler(this.minimalizeBTN2_Click);
            // 
            // closeBTN2
            // 
            this.closeBTN2.Image = global::AukcioKliens.Properties.Resources.close;
            this.closeBTN2.Location = new System.Drawing.Point(504, 3);
            this.closeBTN2.Name = "closeBTN2";
            this.closeBTN2.Size = new System.Drawing.Size(20, 20);
            this.closeBTN2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.closeBTN2.TabIndex = 11;
            this.closeBTN2.TabStop = false;
            this.closeBTN2.Click += new System.EventHandler(this.closeBTN2_Click);
            // 
            // LoginForm
            // 
            this.AcceptButton = this.loginButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.ClientSize = new System.Drawing.Size(780, 330);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.loginPanel);
            this.Controls.Add(this.registrationPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bejelentkezés";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.movingObject_MouseDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.closeBTN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimalizeBTN)).EndInit();
            this.loginPanel.ResumeLayout(false);
            this.loginPanel.PerformLayout();
            this.registrationPanel.ResumeLayout(false);
            this.registrationPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.minimalizeBTN2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeBTN2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox userNameTextBox;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.Button registerButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox closeBTN;
        private System.Windows.Forms.PictureBox minimalizeBTN;
        private System.Windows.Forms.Label label2;
        private Controls.Separator separator1;
        private Controls.Separator separator2;
        private System.Windows.Forms.Button loginButton;
        private System.Windows.Forms.Panel loginPanel;
        private System.Windows.Forms.Panel registrationPanel;
        private System.Windows.Forms.PictureBox minimalizeBTN2;
        private System.Windows.Forms.PictureBox closeBTN2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button registrationButton;
        private System.Windows.Forms.Button backButton;
        private Controls.Separator separator3;
        private System.Windows.Forms.TextBox emailRegtextBox;
        private Controls.Separator separator4;
        private System.Windows.Forms.TextBox usernameRegtextBox;
        private Controls.Separator separator6;
        private System.Windows.Forms.TextBox pwd2RegtextBox;
        private Controls.Separator separator5;
        private System.Windows.Forms.TextBox pwd1RegtextBox;
        private System.Windows.Forms.Label loginErrorLabel;
        private System.Windows.Forms.Label registrationErrorLabel;
    }
}

