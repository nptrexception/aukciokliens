﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Text;
//using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Timers;

namespace AukcioKliens
{
    public partial class MainForm : Form
    {
        static System.Timers.Timer timer;
        public MainForm()
        {
            InitializeComponent();
            AuctionSearchListBox.DrawMode = DrawMode.OwnerDrawFixed;
            AuctionSearchListBox.ItemHeight= (int)(PictureHeight + 2 * ItemMargin);
            AuctionSearchListBox.DrawItem += new DrawItemEventHandler(listBox1_DrawItem);
            AuctionSearchListBox.MeasureItem += new MeasureItemEventHandler(listBox1_MeasureItem);
            OwnAuctionsFlowLayoutPanel.Visible = false;
            OwnAuctionsFlowLayoutPanel.Enabled = false;
            timer = new System.Timers.Timer(10000);
            timer.AutoReset = true;
            timer.Enabled = true;
            timer.Elapsed += OnGetNotification;
            timer.Start();
            //MessageBox.Show("asd\nasd");

        }
        private void CreateHeaders(string endpoint)
        {
            WebClient.Client.DefaultRequestHeaders.Clear();
            WebClient.Client.DefaultRequestHeaders.Add("x-jwt-token", WebClient.token);
            WebClient.Client.DefaultRequestHeaders.Add("x-endpoint-name", endpoint);
        }
        private void OnGetNotification(Object source, ElapsedEventArgs e) 
        {
            
            //get api / notifications / list 
            //get api / notifications / setread
            string url = "notifications" ;
            CreateHeaders("list");
            var result = WebClient.Client.GetAsync(url).Result;
            var contentString = result.Content.ReadAsStringAsync().Result;
            if (result.IsSuccessStatusCode)
            {
                if (contentString != "[]")
                {
                    string message = "";
                    dynamic json = JsonConvert.DeserializeObject(contentString);
                    foreach (var item in json) 
                    {
                        string tmp = item.Message;
                        message += "\n" + tmp;
                    }
                    MessageBox.Show(message);
                    string url2 = "notifications";
                    CreateHeaders("setread");
                    var result2 = WebClient.Client.GetAsync(url2).Result;
                }
            }
            

        }
        [DllImport("user32.dll", EntryPoint = "ReleaseCapture")]
        private static extern void ReleaseCapture();
        [DllImport("user32.dll", EntryPoint = "SendMessage")]
        private static extern void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        private bool checkPassword() 
        {
            var contetntJSON = (dynamic)new JsonObject();
            contetntJSON.Add("password", oldPasswordTextBox.Text);
            var content = new StringContent(contetntJSON.ToString(), Encoding.UTF8, "application/json");
            CreateHeaders("checkpassword");
            var result = WebClient.Client.PostAsync("login", content).Result;
            if (result.IsSuccessStatusCode) 
            {
                string contentString = result.Content.ReadAsStringAsync().Result;
                var json = JsonObject.Parse(contentString);
                if (json["Success"] == true) 
                {
                    return true;
                }
                return true; //mod
            }
            return true; //mod
        }

        public class User
        {
            public int Id { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
            public string Name { get; set; }
            public string Username { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public bool Admin { get; set; }
        }
        private async void registerButton_Click(object sender, EventArgs e)
        {
            try 
            {
                if (newPasswordTextBox.Text == newPassword2TextBox.Text)
                {
                    string userString0 = "users"; // + WebClient.currentUserID;
                    CreateHeaders("getuser");
                    var result0 = await WebClient.Client.PostAsync(userString0, CreateGetIdContent(WebClient.currentUserID));
                    var contentString0 = await result0.Content.ReadAsStringAsync();
                    var json = JsonObject.Parse(contentString0);
                    var contentJSON = new User
                    {
                        Address = addressTextBox.Text,
                        Id = WebClient.currentUserID,
                        Name = nameTextBox.Text,
                        Email = emailTextBox.Text,
                        Password = newPasswordTextBox.Text,
                        Username = json["Username"],
                        Phone = phoneTextbox.Text,
                        Admin = json["Admin"]
                    };
                    /*
                    var contentJSON = (dynamic)new JsonObject();
                    contentJSON.Add("Id", WebClient.currentUserID);
                    contentJSON.Add("Password", newPasswordTextBox.Text);
                    contentJSON.Add("Email", emailTextBox.Text);
                    contentJSON.Add("Name", nameTextBox.Text);
                    contentJSON.Add("Phone", phoneTextbox.Text);
                    contentJSON.Add("Address", addressTextBox.Text);
                    */
                    //???????????????
                    if (checkPassword() == true)
                    {
                        var content = new StringContent(JsonConvert.SerializeObject(contentJSON), Encoding.UTF8, "application/json");
                        string userString = "users";
                        CreateHeaders("modifyuser");
                        var result = WebClient.Client.PostAsync(userString, content).Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var contentString = result.Content.ReadAsStringAsync().Result;
                            MessageBox.Show("Sikeres módosítás!");
                            /* var json = JsonObject.Parse(contentString);

                             if (json["success"] == false)
                             {
                                 MessageBox.Show("Sikertelen!");
                             }
                             else
                             {
                                 MessageBox.Show("Sikeres módosítás!");
                                 this.Close();
                             }*/
                        }
                    }
                    else 
                    {
                        MessageBox.Show("Hibás régi jelszó!");
                    }
                }
                else 
                {
                    MessageBox.Show("A két jelszó különbözik!");
                }               
            }
            catch (System.AggregateException) 
            {
            
            }
        }
        /*
        private void searchPanel_Selected(object sender, EventArgs e)
        {
            if (MainPage.SelectedIndex == 0) 
            {
                try
                {
                    string userString = "/api/Users/" + WebClient.currentUserID;
                    var result = WebClient.Client.GetAsync(userString).Result;
                    var contentString = result.Content.ReadAsStringAsync().Result;
                    var json = JsonObject.Parse(contentString);
                    emailTextBox.Text = json["email"];
                    nameTextBox.Text = json["name"];
                    phoneTextbox.Text = json["phone"];
                    addressTextBox.Text = json["address"];
                    oldPasswordTextBox.Text = "";
                }
                catch (System.AggregateException ex)
                {
                    MessageBox.Show("Error!");
                }
            }
        }
       */
        public class Message
        {
            public string Name = "";
            public string Text { get; set; } = "";
            public int ItemId { get; set; }
            public DateTime Time { get; set; }
            public bool CreateOne { get; set; } = false;
            public Message(string name,string text)
            {
                Name = name;
                Text = text;
            }
        }

        private const int ItemMargin = 5;
        private const float PictureHeight = 100f;

        private void listBox2_DrawItem(object sender, DrawItemEventArgs e)
        {
            // Get the ListBox and the item.
            ListBox lst = sender as ListBox;
            if (lst.Items.Count>0)
            {
                Message message = (Message)lst.Items[e.Index];
                // Draw the background.
                e.DrawBackground();


                // See if the item is selected.
                Brush br;
                if ((e.State & DrawItemState.Selected) ==
                    DrawItemState.Selected)
                    br = SystemBrushes.HighlightText;
                else
                    br = new SolidBrush(e.ForeColor);
                // Find the area in which to put the text.
                float x = e.Bounds.Left + ItemMargin;
                float y = e.Bounds.Top + ItemMargin;
                float width = e.Bounds.Right - ItemMargin - x;
                float height = e.Bounds.Bottom - ItemMargin - y;
                RectangleF layout_rect1 = new RectangleF(x, y, width, height);
                // Draw the text.
                Font f = new Font("Microsoft Sans Serif", 11, FontStyle.Regular);
                string txt = "Név:" + "     " + message.Name + '\n' + "Üzenet:" + "     " + message.Text;
                if (message.CreateOne)
                    txt = "Új hozzászólás";
                e.Graphics.DrawString(txt, f, br, layout_rect1);

                // Outline the text.
                e.Graphics.DrawRectangle(Pens.Black,
                    Rectangle.Round(layout_rect1));

                // Draw the focus rectangle if appropriate.
                e.DrawFocusRectangle();
            }
        }


        private void listBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            // Get the ListBox and the item.
            ListBox lst = sender as ListBox;
            if (lst.Items.Count > 0)
            {
                Auction auction = (Auction)lst.Items[e.Index];
                // Draw the background.
                e.DrawBackground();
                RectangleF source_rect = new RectangleF(0, 0, auction.ThumbnailPicture.Width, auction.ThumbnailPicture.Height);

                float scalew = PictureHeight / auction.ThumbnailPicture.Width;
                // Draw the picture.
                float scale = PictureHeight / auction.ThumbnailPicture.Height;
                float picture_width= PictureHeight;
                float picture_height = PictureHeight; //square
                if (scale>scalew)
                {
                    picture_height = scalew * auction.ThumbnailPicture.Height;
                }
                else
                {
                    picture_width = scale * auction.ThumbnailPicture.Width;
                }
                RectangleF dest_rect = new RectangleF(
                    e.Bounds.Left + ItemMargin+((100f - picture_width) * 0.5f), e.Bounds.Top + ItemMargin+(100f-picture_height)*0.5f,
                    picture_width, picture_height);
                e.Graphics.DrawImage(auction.ThumbnailPicture, dest_rect,
                    source_rect, GraphicsUnit.Pixel);
          
            // See if the item is selected.
            Brush br;
            if ((e.State & DrawItemState.Selected) ==
                DrawItemState.Selected)
                br = SystemBrushes.HighlightText;
            else
                br = new SolidBrush(e.ForeColor);
            // Find the area in which to put the text.
            float x = e.Bounds.Left + PictureHeight + 3 * ItemMargin; //square
            float y = e.Bounds.Top + ItemMargin;
            float width = e.Bounds.Right - ItemMargin - x;
            float height = e.Bounds.Bottom - ItemMargin - y;
            RectangleF layout_rect1 = new RectangleF(x, y, width*0.2f, height);
            RectangleF layout_rect2 = new RectangleF(x+width*0.2f, y, width * 0.4f, height);
            RectangleF layout_rect3 = new RectangleF(x+width*0.6f, y, width * 0.4f, height);
            // Draw the text.
            Font f = new Font("Microsoft Sans Serif", 11, FontStyle.Bold);           
            string txt = '\n'+"     "+"Ár:" + '\n' + "     " + auction.Price;
            e.Graphics.DrawString(txt, f, br, layout_rect1);
            txt = '\n' + "     " + "Név:" +'\n' + "     " + auction.Name +'\n' + "     " + "Kategória:"+'\n' + "     " + auction.Category;
            e.Graphics.DrawString(txt, f, br, layout_rect2);
            string endtext ="Lejár";
            if (auction.EndDate < DateTime.Now)
                endtext += "t";
            endtext += ":";
            txt = '\n' + "     " + endtext + '\n' + "     " + auction.EndDate.ToString() + '\n' + "     " + "Eladó:" + '\n' + "     " + auction.Owner;
            e.Graphics.DrawString(txt, f, br, layout_rect3);

            // Outline the text.
            e.Graphics.DrawRectangle(Pens.Black,

                Rectangle.Round(layout_rect1));
            e.Graphics.DrawRectangle(Pens.Black,
                Rectangle.Round(layout_rect2));
            e.Graphics.DrawRectangle(Pens.Black,
                Rectangle.Round(layout_rect3));
            // Draw the focus rectangle if appropriate.
            e.DrawFocusRectangle();
            }
        }

        private void listBox1_MeasureItem(object sender, MeasureItemEventArgs e)
        {
            e.ItemHeight = (int)(PictureHeight + 2 * ItemMargin);
        }
        private void listBox2_MeasureItem(object sender, MeasureItemEventArgs e)
        {
            e.ItemHeight = (int)(PictureHeight/2 + 2 * ItemMargin);
        }

        public class ItemFilter
        {
            public int? OwnerId { get; set; }
            public string Name { get; set; }
            public int? Price { get; set; } //Jelenlegi ár
            public string Category { get; set; }
            public string Description { get; set; }
        }
        private async void SearchButton_Click(object sender, EventArgs e)
        {
            //api/getitem/id   api/items/getitem/id 
            AuctionSearchListBox.Items.Clear();
            try
            {
                HttpResponseMessage result;
                if (AuctionSearchNameTextBox.Text == "" && AuctionSearchCategoryTextBox.Text == "" && AuctionSearchPriceTextBox.Text == ""
                    && AuctionSearchDescriptionTextBox.Text == "" && !AuctionSearchMyCheckBox.Checked)
                {
                    CreateHeaders("listitems");
                    result = await WebClient.Client.GetAsync("items");
                }
                else
                {
                    var contentJSON = new ItemFilter
                    {

                    };     
                    if (AuctionSearchNameTextBox.Text != "")
                    {
                        contentJSON.Name=AuctionSearchNameTextBox.Text;
                    }
                    if (AuctionSearchCategoryTextBox.Text != "")
                    {
                        contentJSON.Category=AuctionSearchCategoryTextBox.Text;
                    }
                    if (AuctionSearchPriceTextBox.Text != "")
                    {
                        contentJSON.Price=Convert.ToInt32(AuctionSearchPriceTextBox.Text);
                    }
                    if (AuctionSearchDescriptionTextBox.Text != "")
                    {
                        contentJSON.Description=AuctionSearchDescriptionTextBox.Text;
                    }
                    /* Fix?
                    if (AuctionSearchMyCheckBox.Checked)
                    {
                        contentJSON."ownerId", WebClient.currentUserID;
                    }
                    */
                    var content = new StringContent(JsonConvert.SerializeObject(contentJSON), Encoding.UTF8, "application/json");
                    CreateHeaders("filteritems");
                    result = await WebClient.Client.PostAsync("items", content);
                }
                if (result.IsSuccessStatusCode)
                {
                    var contentString = result.Content.ReadAsStringAsync().Result;
                    // var json = JsonObject.Parse(contentString);
                    dynamic json = JsonConvert.DeserializeObject(contentString);
                    foreach (var item in json)
                    {
                        string name = item.Name;
                        Image image;
                        //data:image/jpeg;base64,
                        String simage = item.ThumbnailPicture != ""? item.ThumbnailPicture : "/9j/4AAQSkZJRgABAQAAAQABAAD/7gAOQWRvYmUAZMAAAAAB/9sAQwAQCwsLDAsQDAwQFw8NDxcbFBAQFBsfFxcXFxcfHhcaGhoaFx4eIyUnJSMeLy8zMy8vQEBAQEBAQEBAQEBAQEBA/9sAQwERDw8RExEVEhIVFBEUERQaFBYWFBomGhocGhomMCMeHh4eIzArLicnJy4rNTUwMDU1QEA/QEBAQEBAQEBAQEBA/8AAEQgB8QOEAwEiAAIRAQMRAf/EABkAAQEBAQEBAAAAAAAAAAAAAAAEAwIBBv/EACsQAQABAgQEBgMBAQEAAAAAAAABAgMREzFRBDJhcRIUIUFSgSIzkUKhsf/EABQBAQAAAAAAAAAAAAAAAAAAAAD/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwD7oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHk1UxrMQD0cTetx74uZ4in2iZBqJ54ir2iIczduT7/wFQjmZnWcVcTjETuD0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAczXRGtUOZv0RvINBjPEbU/1zN+udMIBQ8mYjX0SzcrnWqXgKZu24/05m/R7RMpwG08RPtDmb1yffBm9B7NdU6zLl1FuudKZdxYrnXCAZDeOH3q/jqLFEdQTPVUUURpEOL8fhjtIJ1VqcbcJVHDz+MxtINQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZTfiPSIcTfq9oiHNyMK6o6uAdzduT7/wAczMzrOJETOkYuotXJ9v6Dgaxw9XvMQ6jh6feZkGAqizbj2x7uoppjSIgEsU1TpEy6izcn2w7qQGEcPPvLqLFHvMy1AcRatx/l1ERGno9AAAAAHNyMaKo6OifWMARNuHn1mGU+k4O7M4XI6+gKQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAT34wrx3hk34iOWfpgCymcaYneHrizONuOno7AeYxu9SXOeruCrxU7weKneEYCzxU7weKneEYCzxU7weKneEYCzxU7weKneEYCzxU7weKneEYCzxU7weKneEYCzxU7weKneEYDq56Vz3KZwqidpcgLPFTvB4qd4RgLPFTvB4qd4RgLImJ0eseH0nu2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABnfjG32TK64xomOiQG/Dz+MxtLZPYn8pjeFACS5z1d1aS5z1dweRGM4bu8i50c080d4VgmyLnQyLnRvmUfJ5m2/lAMci50Mi50bZtv5QZtv5QDHIudDIudG2bb+UGbb+UAxyLnQyLnRtm2/lBm2/lAMci50Mi50bZtv5QZtv5QDHIudDIudG2bb+UGbb+UAxyLnQyLnRtm2/lBm2/lAMci50Mi50bZtv5Q6pqpq0nEE1VqqmMZ0cKb/6/tMDfh9J7tmPD6T3bAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAI5jCZjZYluxhckC1OFyP4qRxOExOywBJc56u6tJc56u4FPNHeFaSnmjvCsEc6vHs6vAHXhq1wnDs3tW4piJnmn/jQEQpu24qiao5o/wCpgAAAAAAAAG3D61MW3D61A7v/AK/tMpv/AK/tMDfh9J7tmPD6T3bAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJ+Ij8oneFDLiI/GJ2kE6uicaInokU2Jxo7SDRJc56u6tJc56u4FPNHeFaSnmjvCsEc6hOrwFoxtXYiPDV9S18VOuMA9Rtrt2Jjw0/csAAAAegREzOEatfLzhr6+8O7VvwxjPN/40BHMTE4TrDxTdt+OMY5oTg8bcPrUxbcPrUDu/8Ar+0ym/8Ar+0wN+H0nu2Y8PpPdsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4vRjbnp6u3lUY0zG8Ajb8PPNH2waWJwrw3gFKS5z1d1aS5z1dwKeaO8K0lPNHeFYI51ePZ1eAAAAAAAKLVrD8qtfaHNm1/qr6huAAAyvW8fyp194agIm3D61F63h+VOnvBw+tQO7/6/tMpv/r+0wN+H0nu2Y8PpPdsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACSqMKpjqW5wrpnq6vRhcnq4BYkuc9XdVE4xE7pbnPV3Ap5o7wrSU80d4VgjnV49nV4ANKLVVfrpG7em3TTGER3BINq7HvR/GUxMekg8a2rXi/KrT2jd5at+KcZ5Y/6pAAAAAAAcUW/BVMxpLsBnf/AF/aZTf/AF/aYG/D6T3bMeH0nu2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABhxEesSxUX4/CJ2lOCq1ONuE9znq7trE/hMbSxuc9XcCnmjvCtJTzR3hWCPCZnCPWW9FmI9avWdndNNNOkOgAAHNVFNesfboB5EREYRo9AAAAAAAAAGd/9f2mU3/1/aYG/D6T3bMeH0nu2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABxdjG3UlWTGMTG6QCJmNJweOooqnSJeTExOE6g9p5o7wrRxOExOzbzEbSDYY+YjaTzEbSDYY+YjaTzEbSDYY+YjaTzEbSDYY+YjaTzEbSDYY+YjaTzEbSDYY+YjaTzEbSDYY+YjaTzEbSDYY+YjaTzEbSDq/wDr+0zW5diunDDBkDfh9J7tmPD6T3bAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPIiI0jB6AJLnPV3VuJtUTOMx6yCUU5NvYybewJhTk29jJt7AmFOTb2Mm3sCYU5NvYybewJhTk29jJt7AmFOTb2Mm3sCYU5NvYybewJhTk29jJt7AmFOTb2Mm3sCYU5NvYybewOeH0nu2c00U08vu6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//2Q==";
                        var imgArr = Convert.FromBase64String(simage);
                        using (var ms = new MemoryStream(imgArr))
                        {
                            image = Image.FromStream(ms);
                        }
                        string category = item.Category;
                        int ownerid = item.OwnerId;
                        string ownersting = "users";// + ownerid.ToString();
                        CreateHeaders("getuser");
                        var result2 = await WebClient.Client.PostAsync(ownersting, CreateGetIdContent(ownerid));
                        var contentString2 = await result2.Content.ReadAsStringAsync();
                        var xc = WebClient.currentUserID;
                        string owner = "";
                        try {
                            var json2 = JsonObject.Parse(contentString2);
                            owner = json2["Name"];
                        }
                        catch (Exception) { }
                        int price = item.Price;

                        string dateString = item.EndDate;
                        DateTime endDate = DateTime.Parse(dateString);
                        int id = item.Id;
                        Auction a = new Auction(id, name, image, category, owner, price, endDate);
                        a.OwnerId = ownerid;
                        a.Star = item.Star;
                        a.StarDescription = item.StarDescription;
                        a.Minprice = item.Minprice;
                        dateString = item.StartDate;
                        a.StartDate = DateTime.Parse(dateString);
                        a.Description = item.Description;
                        a.FinalPrice = item.FinalPrice;
                        string bidownerid = item.BidOwnerId;
                        a.BidOwnerId = 0;
                        try { 
                            a.BidOwnerId = Convert.ToInt32(bidownerid);
                        }
                        catch(Exception)
                        { }
                        if (bidownerid==null || bidownerid == "0" || bidownerid == "" || bidownerid == "null")
                            a.BidOwner = null;
                        else
                        {
                            string ownersting2 = "users";
                            CreateHeaders("getuser");
                            if (item.BidOwner != null)
                            {
                                var result3 = await WebClient.Client.PostAsync(ownersting2, CreateGetIdContent((int)item.BidOwner));
                                var contentString3 = result3.Content.ReadAsStringAsync().Result;
                                string owner2 = "";
                                try
                                {
                                    var json3 = JsonObject.Parse(contentString3);
                                    owner2 = json3["Name"];
                                }
                                catch (Exception) { }
                                a.BidOwner = owner2;
                            }
                            else
                                a.BidOwner = "";
                        }
                        a.MaxBid = 0;
                        string s = item.maxBid;
                        if (s != "null")
                            a.MaxBid = Convert.ToInt32(s);
                        a.WinnerId = null;
                        try
                        {
                            s = item.WinningBidId;
                            a.WinnerId = Convert.ToInt32(s);
                        }
                        catch (Exception)
                        { }
                        if ( a.WinnerId==null)
                            a.WinningBid = null;
                        else
                        {
                            string ownersting2 = "users";
                            CreateHeaders("getuser");
                            var result3 = await WebClient.Client.PostAsync(ownersting2, CreateGetIdContent(a.WinnerId.Value));
                            var contentString3 = await result3.Content.ReadAsStringAsync();
                            string owner2 = "";
                            try
                            {
                                var json3 = JsonObject.Parse(contentString3);
                                owner2 = json3["Name"];
                            }
                            catch (Exception) { }
                            a.WinningBid = owner2;
                            a.WinnerId = item.winningBidId;
                        }
                        if (AuctionSearchMyCheckBox.Checked)
                        {
                            if (a.BidOwnerId == WebClient.currentUserID || a.OwnerId == WebClient.currentUserID || a.WinnerId == WebClient.currentUserID)
                                AuctionSearchListBox.Items.Add(a);
                        }
                        else
                            AuctionSearchListBox.Items.Add(a);

                    }
                }
                else 
                {
                    MessageBox.Show("Error!");
                }
            }
            catch (System.AggregateException) 
            {
                MessageBox.Show("Error!");
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //WebClient.Client.GetAsync("api/login/logout").Wait(); ????
            WebClient.currentUserID = -1;
            Application.Exit();
        }
        private StringContent CreateGetIdContent(int id)
        {
            var userIDObject = new
            {
                Id = id
            };
            string stringcontent = JsonConvert.SerializeObject(userIDObject); 
            return new StringContent(stringcontent, Encoding.UTF8, "application/json");
        }

        private async void MainForm_Load(object sender, EventArgs e)
        { 
            try
            {
                
                string userString = "users"; // + WebClient.currentUserID;
                CreateHeaders("getuser");
                var result = await WebClient.Client.PostAsync(userString, CreateGetIdContent(WebClient.currentUserID));           
                var contentString = await result.Content.ReadAsStringAsync();
                var json = JsonObject.Parse(contentString);
                emailTextBox.Text = json["Email"];
                nameTextBox.Text = json["Name"];
                phoneTextbox.Text = json["Phone"];
                addressTextBox.Text = json["Address"];
                oldPasswordTextBox.Text = "";
                
            }
            catch (System.AggregateException) 
            {
                MessageBox.Show("Error!");
            }
        }

        private void AuctionSearchListBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (AuctionSearchListBox.Items.Count > 0)
            {
                ListBox l = (ListBox)sender;
                Auction a = (Auction)l.SelectedItem;
                AuctionForm af = new AuctionForm(a);
                af.Show();
                af.AuctionPanelInit();
                af.ShowAuctionBid();
            }
        }
        private string ImagetoBase64(Image image)
        {
            byte[] imageByteArray;
            using (var ms = new MemoryStream())
            {
                image.Save(ms, image.RawFormat);
                imageByteArray = ms.ToArray();
            }
            return Convert.ToBase64String(imageByteArray);
        }

        private Image Base64toImage(string bs64)
        {
            byte[] byteArrya = Convert.FromBase64String(bs64);
            Image returnImage = null;
            try
            {
                MemoryStream ms = new MemoryStream(byteArrya, 0, byteArrya.Length);
                ms.Write(byteArrya, 0, byteArrya.Length);
                returnImage = Image.FromStream(ms, true);
            }
            catch { }
            return returnImage;
        }


        private void SearchCreateAction_Click(object sender, EventArgs e)
        {
            ShowAuctionInOtherForm();
        }

        private void ShowAuctionInOtherForm()
        {
            AuctionForm af = new AuctionForm();
            af.Show();
            af.AuctionPanelInit();
            af.ShowAuctionCreate();
        }

        
        private void SearchPanelInit()
        {
            AuctionSearchMyCheckBox.Checked = false;
            AuctionSearchNameTextBox.Text = "";
            AuctionSearchCategoryTextBox.Text = "";
            AuctionSearchPriceTextBox.Text = "";
            AuctionSearchDescriptionTextBox.Text = "";
            SearchInputFlowLayoutPanel.Visible = true;
            SearchInputFlowLayoutPanel.Enabled = true;
            bool admin = false;
            if (admin)
            {
                SearchCreateAction.Enabled = false;
                SearchCreateAction.Visible = false;
            }
        }

        private void menuBTN_Click(object sender, EventArgs e)
        {
            if (sideMenuPanel.Width == 250)
            {
                label5.Visible = true;
                label4.Visible = false;
                sideMenuPanel.Width = 83;
            }
            else 
            {
                label5.Visible = false;
                label4.Visible = true;
                sideMenuPanel.Width = 250;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
            this.WindowState = FormWindowState.Maximized;
            pictureBox4.Visible = true;
            pictureBox3.Visible = false;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            pictureBox4.Visible = false;
            pictureBox3.Visible = true;
        }

        private void profilButton_Click(object sender, EventArgs e)
        {
            profilPanel.Visible = true;
            SearchFlowLayoutPanel.Visible = false;
        }

        private void productsButton_Click(object sender, EventArgs e)
        {
            profilPanel.Visible = false;
            SearchFlowLayoutPanel.Visible = true;
            OwnAuctionsFlowLayoutPanel.Visible = false;
            OwnAuctionsFlowLayoutPanel.Enabled = false;
            SearchInputFlowLayoutPanel.Visible = true;
            SearchInputFlowLayoutPanel.Enabled = true;
        }

        private void ownProductsButton_Click(object sender, EventArgs e)
        {
            profilPanel.Visible = false;
            SearchFlowLayoutPanel.Visible = true;
            OwnAuctionsFlowLayoutPanel.Visible = true;
            OwnAuctionsFlowLayoutPanel.Enabled = true;
            SearchInputFlowLayoutPanel.Visible = false;
            SearchInputFlowLayoutPanel.Enabled = false;

        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            pictureBox1.BackColor = Color.DarkRed;
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            pictureBox1.BackColor = pictureBox1.Parent.BackColor;
        }
        private void pictureBox2_MouseEnter(object sender, EventArgs e)
        {
            pictureBox2.BackColor = Color.DarkRed;
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            pictureBox2.BackColor = pictureBox1.Parent.BackColor;
        }
        private void pictureBox3_MouseEnter(object sender, EventArgs e)
        {
            pictureBox3.BackColor = Color.DarkRed;
        }

        private void pictureBox3_MouseLeave(object sender, EventArgs e)
        {
            pictureBox3.BackColor = pictureBox3.Parent.BackColor;
        }

        private async void OwnAuctionsButton_Click(object sender, EventArgs e)
        {
            HttpResponseMessage result;
            CreateHeaders("listitems");
            result = await WebClient.Client.GetAsync("items");
            if (result.IsSuccessStatusCode)
            {
                AuctionSearchListBox.Items.Clear();
                var contentString = await result.Content.ReadAsStringAsync();
                // var json = JsonObject.Parse(contentString);
                dynamic json = JsonConvert.DeserializeObject(contentString);
                foreach (var item in json)
                {
                    string name = item.Name;
                    Image image;
                    String simage = item.ThumbnailPicture;
                   
                    var imgArr = Convert.FromBase64String(simage);
                    using (var ms = new MemoryStream(imgArr))
                    {
                        image = Image.FromStream(ms);
                    }
                    string category = item.Category;
                    int ownerid = item.OwnerId;
                    string ownersting = "users";// + ownerid.ToString();
                    CreateHeaders("getuser");
                    var result2 = await WebClient.Client.PostAsync(ownersting,CreateGetIdContent(ownerid));
                    var contentString2 = await result2.Content.ReadAsStringAsync();
                    var xc = WebClient.currentUserID;
                    string owner = "";
                    try
                    {
                        var json2 = JsonObject.Parse(contentString2);
                        owner = json2["Name"];
                    }
                    catch (Exception) { }
                    int price = item.Price;

                    string dateString = item.EndDate;
                    DateTime endDate = DateTime.Parse(dateString);
                    int id = item.Id;
                    Auction a = new Auction(id, name, image, category, owner, price, endDate);
                    a.OwnerId = ownerid;
                    a.Star = item.Star;
                    a.StarDescription = item.StarDescription;
                    a.Minprice = item.MinPrice??0;
                    dateString = item.StartDate;
                    a.StartDate = DateTime.Parse(dateString);
                    a.Description = item.Description;
                    a.FinalPrice = item.FinalPrice;
                    string bidownerid = item.BidOwnerId;
                    a.BidOwnerId = 0;
                    try
                    {
                        a.BidOwnerId = Convert.ToInt32(bidownerid);
                    }
                    catch (Exception)
                    { }
                    if (bidownerid == null || bidownerid == "0" || bidownerid == "" || bidownerid == "null")
                        a.BidOwner = null;
                    else
                    {
                        string ownersting2 = "users";// + bidownerid;
                        CreateHeaders("getuser");
                        if (item.BidOwnerId != null)
                        {
                            var result3 = await WebClient.Client.PostAsync(ownersting2, CreateGetIdContent((int)item.BidOwnerId));
                            var contentString3 = await result3.Content.ReadAsStringAsync();
                            string owner2 = "";
                            try
                            {
                                var json3 = JsonObject.Parse(contentString3);
                                owner2 = json3["Name"];
                            }
                            catch (Exception) { }
                            a.BidOwner = owner2;
                        }
                        else
                            a.BidOwner = "";
                    }
                    a.MaxBid = 0;
                    string s = item.MaxBid;
                    if (s != "null")
                        a.MaxBid = Convert.ToInt32(s);
                    a.WinnerId = null;
                    try
                    {
                        s = item.WinningBidId;
                        a.WinnerId = Convert.ToInt32(s);
                    }
                    catch (Exception)
                    { }
                    if (a.WinnerId == null)
                        a.WinningBid = null;
                    else
                    {
                        string ownersting2 = "users";// + item.winningBidId;
                        CreateHeaders("getuser");
                        if (item.WinningBidId != null)
                        {
                            var result3 = await WebClient.Client.PostAsync(ownersting2, CreateGetIdContent((int)item.WinningBidId));
                            var contentString3 = await result3.Content.ReadAsStringAsync();
                            string owner2 = "";
                            try
                            {
                                var json3 = JsonObject.Parse(contentString3);
                                owner2 = json3["Name"];
                            }
                            catch (Exception) { }
                            a.WinningBid = owner2;
                            a.WinnerId = item.WinningBidId;
                        }
                    }
                    if (a.BidOwnerId == WebClient.currentUserID || a.OwnerId == WebClient.currentUserID || a.WinnerId == WebClient.currentUserID)
                        AuctionSearchListBox.Items.Add(a);

                }
            }
            else
            {
                MessageBox.Show("Error!");
            }

        }
    }
    public class Auction
    {
        public string Owner { get; set; } = "";
        public int OwnerId { get; set; }
        public string BidOwner { get; set; } = "";
        public int BidOwnerId { get; set; }
        public Image Picture { get; set; } = null;
        public int Id { get; set; }
        public int? MaxBid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Image ThumbnailPicture { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? Price { get; set; }
        public int Minprice { get; set; }
        public string Category { get; set; }
        public int? FinalPrice { get; set; }
        public string WinningBid { get; set; }
        public int? WinnerId { get; set; }
        public int? Star { get; set; }
        public string StarDescription { get; set; }

        public Auction(int id, string name, Image thpicture, string category, string owner, int price, DateTime enddate)
        {
            Id = id;
            Name = name;
            Category = category;
            Owner = owner;
            ThumbnailPicture = thpicture;
            EndDate = enddate;
            Price = price;
        }
    }
}
