﻿namespace AukcioKliens
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SearchFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.AuctionSearchNameTextBox = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.AuctionSearchCategoryTextBox = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.AuctionSearchPriceTextBox = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.AuctionSearchDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.AuctionSearchMyCheckBox = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.AuctionSearchButton = new System.Windows.Forms.Button();
            this.SearchCreateAction = new System.Windows.Forms.Button();
            this.AuctionSearchListBox = new System.Windows.Forms.ListBox();
            this.confirmButton = new System.Windows.Forms.Button();
            this.oldPasswordTextBox = new System.Windows.Forms.TextBox();
            this.newPassword2TextBox = new System.Windows.Forms.TextBox();
            this.addressTextBox = new System.Windows.Forms.TextBox();
            this.phoneTextbox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.newPasswordTextBox = new System.Windows.Forms.TextBox();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.oldPasswordLabel = new System.Windows.Forms.Label();
            this.newPassword2Label = new System.Windows.Forms.Label();
            this.addressLabel = new System.Windows.Forms.Label();
            this.phoneLabel = new System.Windows.Forms.Label();
            this.newPasswordLabel = new System.Windows.Forms.Label();
            this.emailLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.sideMenuPanel = new System.Windows.Forms.Panel();
            this.startPageButton = new System.Windows.Forms.Button();
            this.ownProductsButton = new System.Windows.Forms.Button();
            this.profilButton = new System.Windows.Forms.Button();
            this.productsButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.headPanel = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuBTN = new System.Windows.Forms.PictureBox();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.profilPanel = new System.Windows.Forms.Panel();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SearchInputFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.OwnAuctionsFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.OwnAuctionsButton = new System.Windows.Forms.Button();
            this.SearchFlowLayoutPanel.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            this.flowLayoutPanel8.SuspendLayout();
            this.flowLayoutPanel7.SuspendLayout();
            this.sideMenuPanel.SuspendLayout();
            this.headPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.menuBTN)).BeginInit();
            this.mainPanel.SuspendLayout();
            this.profilPanel.SuspendLayout();
            this.SearchInputFlowLayoutPanel.SuspendLayout();
            this.OwnAuctionsFlowLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // SearchFlowLayoutPanel
            // 
            this.SearchFlowLayoutPanel.Controls.Add(this.OwnAuctionsFlowLayoutPanel);
            this.SearchFlowLayoutPanel.Controls.Add(this.SearchInputFlowLayoutPanel);
            this.SearchFlowLayoutPanel.Controls.Add(this.AuctionSearchListBox);
            this.SearchFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SearchFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.SearchFlowLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.SearchFlowLayoutPanel.Name = "SearchFlowLayoutPanel";
            this.SearchFlowLayoutPanel.Size = new System.Drawing.Size(895, 750);
            this.SearchFlowLayoutPanel.TabIndex = 21;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.AuctionSearchNameTextBox);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 36);
            this.flowLayoutPanel1.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Termék neve:";
            // 
            // AuctionSearchNameTextBox
            // 
            this.AuctionSearchNameTextBox.Location = new System.Drawing.Point(3, 16);
            this.AuctionSearchNameTextBox.Name = "AuctionSearchNameTextBox";
            this.AuctionSearchNameTextBox.Size = new System.Drawing.Size(197, 20);
            this.AuctionSearchNameTextBox.TabIndex = 0;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.label2);
            this.flowLayoutPanel2.Controls.Add(this.AuctionSearchCategoryTextBox);
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 45);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(200, 39);
            this.flowLayoutPanel2.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Termék kategóriája:";
            // 
            // AuctionSearchCategoryTextBox
            // 
            this.AuctionSearchCategoryTextBox.Location = new System.Drawing.Point(3, 16);
            this.AuctionSearchCategoryTextBox.Name = "AuctionSearchCategoryTextBox";
            this.AuctionSearchCategoryTextBox.Size = new System.Drawing.Size(197, 20);
            this.AuctionSearchCategoryTextBox.TabIndex = 1;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.label3);
            this.flowLayoutPanel3.Controls.Add(this.AuctionSearchPriceTextBox);
            this.flowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(3, 90);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(200, 39);
            this.flowLayoutPanel3.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Termék ára:";
            // 
            // AuctionSearchPriceTextBox
            // 
            this.AuctionSearchPriceTextBox.Location = new System.Drawing.Point(3, 16);
            this.AuctionSearchPriceTextBox.Name = "AuctionSearchPriceTextBox";
            this.AuctionSearchPriceTextBox.Size = new System.Drawing.Size(197, 20);
            this.AuctionSearchPriceTextBox.TabIndex = 2;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Controls.Add(this.label6);
            this.flowLayoutPanel6.Controls.Add(this.AuctionSearchDescriptionTextBox);
            this.flowLayoutPanel6.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(3, 135);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(200, 39);
            this.flowLayoutPanel6.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Termék leírása:";
            // 
            // AuctionSearchDescriptionTextBox
            // 
            this.AuctionSearchDescriptionTextBox.Location = new System.Drawing.Point(3, 16);
            this.AuctionSearchDescriptionTextBox.Name = "AuctionSearchDescriptionTextBox";
            this.AuctionSearchDescriptionTextBox.Size = new System.Drawing.Size(197, 20);
            this.AuctionSearchDescriptionTextBox.TabIndex = 5;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.Controls.Add(this.AuctionSearchMyCheckBox);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(3, 180);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(200, 39);
            this.flowLayoutPanel8.TabIndex = 19;
            // 
            // AuctionSearchMyCheckBox
            // 
            this.AuctionSearchMyCheckBox.AutoSize = true;
            this.AuctionSearchMyCheckBox.Location = new System.Drawing.Point(3, 3);
            this.AuctionSearchMyCheckBox.Name = "AuctionSearchMyCheckBox";
            this.AuctionSearchMyCheckBox.Size = new System.Drawing.Size(85, 17);
            this.AuctionSearchMyCheckBox.TabIndex = 18;
            this.AuctionSearchMyCheckBox.Text = "Saját aukció";
            this.AuctionSearchMyCheckBox.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.Controls.Add(this.AuctionSearchButton);
            this.flowLayoutPanel7.Controls.Add(this.SearchCreateAction);
            this.flowLayoutPanel7.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(3, 225);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(200, 38);
            this.flowLayoutPanel7.TabIndex = 16;
            // 
            // AuctionSearchButton
            // 
            this.AuctionSearchButton.Location = new System.Drawing.Point(3, 3);
            this.AuctionSearchButton.Name = "AuctionSearchButton";
            this.AuctionSearchButton.Size = new System.Drawing.Size(95, 23);
            this.AuctionSearchButton.TabIndex = 0;
            this.AuctionSearchButton.Text = "Keresés";
            this.AuctionSearchButton.UseVisualStyleBackColor = true;
            this.AuctionSearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // SearchCreateAction
            // 
            this.SearchCreateAction.Location = new System.Drawing.Point(104, 3);
            this.SearchCreateAction.Name = "SearchCreateAction";
            this.SearchCreateAction.Size = new System.Drawing.Size(96, 23);
            this.SearchCreateAction.TabIndex = 1;
            this.SearchCreateAction.Text = "Új létrehozása";
            this.SearchCreateAction.UseVisualStyleBackColor = true;
            this.SearchCreateAction.Click += new System.EventHandler(this.SearchCreateAction_Click);
            // 
            // AuctionSearchListBox
            // 
            this.AuctionSearchListBox.FormattingEnabled = true;
            this.AuctionSearchListBox.Location = new System.Drawing.Point(214, 3);
            this.AuctionSearchListBox.Name = "AuctionSearchListBox";
            this.AuctionSearchListBox.Size = new System.Drawing.Size(601, 563);
            this.AuctionSearchListBox.TabIndex = 20;
            this.AuctionSearchListBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.AuctionSearchListBox_MouseDoubleClick);
            // 
            // confirmButton
            // 
            this.confirmButton.Location = new System.Drawing.Point(160, 321);
            this.confirmButton.Name = "confirmButton";
            this.confirmButton.Size = new System.Drawing.Size(150, 23);
            this.confirmButton.TabIndex = 25;
            this.confirmButton.Text = "Megerősítés";
            this.confirmButton.UseVisualStyleBackColor = true;
            this.confirmButton.Click += new System.EventHandler(this.registerButton_Click);
            // 
            // oldPasswordTextBox
            // 
            this.oldPasswordTextBox.Location = new System.Drawing.Point(134, 267);
            this.oldPasswordTextBox.Name = "oldPasswordTextBox";
            this.oldPasswordTextBox.Size = new System.Drawing.Size(176, 20);
            this.oldPasswordTextBox.TabIndex = 29;
            this.oldPasswordTextBox.UseSystemPasswordChar = true;
            // 
            // newPassword2TextBox
            // 
            this.newPassword2TextBox.Location = new System.Drawing.Point(133, 141);
            this.newPassword2TextBox.Name = "newPassword2TextBox";
            this.newPassword2TextBox.Size = new System.Drawing.Size(176, 20);
            this.newPassword2TextBox.TabIndex = 27;
            this.newPassword2TextBox.UseSystemPasswordChar = true;
            // 
            // addressTextBox
            // 
            this.addressTextBox.Location = new System.Drawing.Point(133, 235);
            this.addressTextBox.Name = "addressTextBox";
            this.addressTextBox.Size = new System.Drawing.Size(176, 20);
            this.addressTextBox.TabIndex = 24;
            // 
            // phoneTextbox
            // 
            this.phoneTextbox.Location = new System.Drawing.Point(133, 204);
            this.phoneTextbox.Name = "phoneTextbox";
            this.phoneTextbox.Size = new System.Drawing.Size(176, 20);
            this.phoneTextbox.TabIndex = 23;
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(133, 172);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(176, 20);
            this.nameTextBox.TabIndex = 22;
            // 
            // newPasswordTextBox
            // 
            this.newPasswordTextBox.Location = new System.Drawing.Point(133, 109);
            this.newPasswordTextBox.Name = "newPasswordTextBox";
            this.newPasswordTextBox.Size = new System.Drawing.Size(176, 20);
            this.newPasswordTextBox.TabIndex = 21;
            this.newPasswordTextBox.UseSystemPasswordChar = true;
            // 
            // emailTextBox
            // 
            this.emailTextBox.Location = new System.Drawing.Point(133, 79);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(176, 20);
            this.emailTextBox.TabIndex = 20;
            // 
            // oldPasswordLabel
            // 
            this.oldPasswordLabel.AutoSize = true;
            this.oldPasswordLabel.Location = new System.Drawing.Point(41, 270);
            this.oldPasswordLabel.Name = "oldPasswordLabel";
            this.oldPasswordLabel.Size = new System.Drawing.Size(61, 13);
            this.oldPasswordLabel.TabIndex = 28;
            this.oldPasswordLabel.Text = "Régi jelszó:";
            // 
            // newPassword2Label
            // 
            this.newPassword2Label.AutoSize = true;
            this.newPassword2Label.Location = new System.Drawing.Point(40, 144);
            this.newPassword2Label.Name = "newPassword2Label";
            this.newPassword2Label.Size = new System.Drawing.Size(69, 13);
            this.newPassword2Label.TabIndex = 26;
            this.newPassword2Label.Text = "Új jelszó újra:";
            // 
            // addressLabel
            // 
            this.addressLabel.AutoSize = true;
            this.addressLabel.Location = new System.Drawing.Point(40, 238);
            this.addressLabel.Name = "addressLabel";
            this.addressLabel.Size = new System.Drawing.Size(46, 13);
            this.addressLabel.TabIndex = 19;
            this.addressLabel.Text = "Lakcím:";
            // 
            // phoneLabel
            // 
            this.phoneLabel.AutoSize = true;
            this.phoneLabel.Location = new System.Drawing.Point(40, 207);
            this.phoneLabel.Name = "phoneLabel";
            this.phoneLabel.Size = new System.Drawing.Size(70, 13);
            this.phoneLabel.TabIndex = 18;
            this.phoneLabel.Text = "Telefonszám:";
            // 
            // newPasswordLabel
            // 
            this.newPasswordLabel.AutoSize = true;
            this.newPasswordLabel.Location = new System.Drawing.Point(40, 112);
            this.newPasswordLabel.Name = "newPasswordLabel";
            this.newPasswordLabel.Size = new System.Drawing.Size(49, 13);
            this.newPasswordLabel.TabIndex = 17;
            this.newPasswordLabel.Text = "Új jelszó:";
            // 
            // emailLabel
            // 
            this.emailLabel.AutoSize = true;
            this.emailLabel.Location = new System.Drawing.Point(40, 82);
            this.emailLabel.Name = "emailLabel";
            this.emailLabel.Size = new System.Drawing.Size(59, 13);
            this.emailLabel.TabIndex = 16;
            this.emailLabel.Text = "E-mail cím:";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(40, 175);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(30, 13);
            this.nameLabel.TabIndex = 13;
            this.nameLabel.Text = "Név:";
            // 
            // sideMenuPanel
            // 
            this.sideMenuPanel.BackColor = System.Drawing.Color.DarkRed;
            this.sideMenuPanel.Controls.Add(this.startPageButton);
            this.sideMenuPanel.Controls.Add(this.ownProductsButton);
            this.sideMenuPanel.Controls.Add(this.profilButton);
            this.sideMenuPanel.Controls.Add(this.productsButton);
            this.sideMenuPanel.Controls.Add(this.label5);
            this.sideMenuPanel.Controls.Add(this.label4);
            this.sideMenuPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.sideMenuPanel.Location = new System.Drawing.Point(0, 0);
            this.sideMenuPanel.Name = "sideMenuPanel";
            this.sideMenuPanel.Size = new System.Drawing.Size(250, 800);
            this.sideMenuPanel.TabIndex = 1;
            // 
            // startPageButton
            // 
            this.startPageButton.FlatAppearance.BorderSize = 0;
            this.startPageButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.startPageButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.startPageButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startPageButton.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.startPageButton.Image = global::AukcioKliens.Properties.Resources.output_onlinepngtools__19_;
            this.startPageButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.startPageButton.Location = new System.Drawing.Point(0, 132);
            this.startPageButton.Name = "startPageButton";
            this.startPageButton.Padding = new System.Windows.Forms.Padding(17, 0, 0, 0);
            this.startPageButton.Size = new System.Drawing.Size(250, 45);
            this.startPageButton.TabIndex = 7;
            this.startPageButton.Text = "Kezdőlap";
            this.startPageButton.UseVisualStyleBackColor = true;
            // 
            // ownProductsButton
            // 
            this.ownProductsButton.FlatAppearance.BorderSize = 0;
            this.ownProductsButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.ownProductsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ownProductsButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ownProductsButton.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.ownProductsButton.Image = global::AukcioKliens.Properties.Resources.output_onlinepngtools__18_;
            this.ownProductsButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ownProductsButton.Location = new System.Drawing.Point(0, 278);
            this.ownProductsButton.Name = "ownProductsButton";
            this.ownProductsButton.Padding = new System.Windows.Forms.Padding(17, 0, 0, 0);
            this.ownProductsButton.Size = new System.Drawing.Size(250, 45);
            this.ownProductsButton.TabIndex = 6;
            this.ownProductsButton.Text = "         Saját hirdetéseim";
            this.ownProductsButton.UseVisualStyleBackColor = true;
            this.ownProductsButton.Click += new System.EventHandler(this.ownProductsButton_Click);
            // 
            // profilButton
            // 
            this.profilButton.FlatAppearance.BorderSize = 0;
            this.profilButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.profilButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.profilButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profilButton.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.profilButton.Image = global::AukcioKliens.Properties.Resources.output_onlinepngtools__13_;
            this.profilButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.profilButton.Location = new System.Drawing.Point(0, 227);
            this.profilButton.Name = "profilButton";
            this.profilButton.Padding = new System.Windows.Forms.Padding(14, 0, 0, 0);
            this.profilButton.Size = new System.Drawing.Size(250, 45);
            this.profilButton.TabIndex = 5;
            this.profilButton.Text = "Profil";
            this.profilButton.UseVisualStyleBackColor = true;
            this.profilButton.Click += new System.EventHandler(this.profilButton_Click);
            // 
            // productsButton
            // 
            this.productsButton.FlatAppearance.BorderSize = 0;
            this.productsButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.productsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.productsButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productsButton.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.productsButton.Image = global::AukcioKliens.Properties.Resources.output_onlinepngtools__11_;
            this.productsButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.productsButton.Location = new System.Drawing.Point(0, 176);
            this.productsButton.Name = "productsButton";
            this.productsButton.Padding = new System.Windows.Forms.Padding(17, 0, 0, 0);
            this.productsButton.Size = new System.Drawing.Size(250, 45);
            this.productsButton.TabIndex = 4;
            this.productsButton.Text = "Termékek";
            this.productsButton.UseVisualStyleBackColor = true;
            this.productsButton.Click += new System.EventHandler(this.productsButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Algerian", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 108);
            this.label5.TabIndex = 3;
            this.label5.Text = "NP\r\nTR";
            this.label5.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.label4.Font = new System.Drawing.Font("Algerian", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 54);
            this.label4.TabIndex = 2;
            this.label4.Text = "NPTR";
            // 
            // headPanel
            // 
            this.headPanel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.headPanel.Controls.Add(this.pictureBox4);
            this.headPanel.Controls.Add(this.pictureBox3);
            this.headPanel.Controls.Add(this.pictureBox2);
            this.headPanel.Controls.Add(this.pictureBox1);
            this.headPanel.Controls.Add(this.menuBTN);
            this.headPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headPanel.Location = new System.Drawing.Point(250, 0);
            this.headPanel.Name = "headPanel";
            this.headPanel.Size = new System.Drawing.Size(950, 50);
            this.headPanel.TabIndex = 2;
            this.headPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox4.Image = global::AukcioKliens.Properties.Resources.output_onlinepngtools__17_;
            this.pictureBox4.Location = new System.Drawing.Point(901, 3);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(20, 20);
            this.pictureBox4.TabIndex = 4;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Visible = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Image = global::AukcioKliens.Properties.Resources.output_onlinepngtools__15_;
            this.pictureBox3.Location = new System.Drawing.Point(901, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(20, 20);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            this.pictureBox3.MouseEnter += new System.EventHandler(this.pictureBox3_MouseEnter);
            this.pictureBox3.MouseLeave += new System.EventHandler(this.pictureBox3_MouseLeave);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.Image = global::AukcioKliens.Properties.Resources.min;
            this.pictureBox2.Location = new System.Drawing.Point(875, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(20, 20);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            this.pictureBox2.MouseEnter += new System.EventHandler(this.pictureBox2_MouseEnter);
            this.pictureBox2.MouseLeave += new System.EventHandler(this.pictureBox2_MouseLeave);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::AukcioKliens.Properties.Resources.close;
            this.pictureBox1.Location = new System.Drawing.Point(927, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(20, 20);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.pictureBox1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            // 
            // menuBTN
            // 
            this.menuBTN.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuBTN.Image = global::AukcioKliens.Properties.Resources.output_onlinepngtools__10_;
            this.menuBTN.Location = new System.Drawing.Point(6, 9);
            this.menuBTN.Name = "menuBTN";
            this.menuBTN.Size = new System.Drawing.Size(35, 35);
            this.menuBTN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.menuBTN.TabIndex = 0;
            this.menuBTN.TabStop = false;
            this.menuBTN.Click += new System.EventHandler(this.menuBTN_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.mainPanel.Controls.Add(this.profilPanel);
            this.mainPanel.Controls.Add(this.SearchFlowLayoutPanel);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.mainPanel.Location = new System.Drawing.Point(250, 50);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(895, 750);
            this.mainPanel.TabIndex = 3;
            // 
            // profilPanel
            // 
            this.profilPanel.Controls.Add(this.confirmButton);
            this.profilPanel.Controls.Add(this.oldPasswordTextBox);
            this.profilPanel.Controls.Add(this.emailTextBox);
            this.profilPanel.Controls.Add(this.newPassword2TextBox);
            this.profilPanel.Controls.Add(this.nameLabel);
            this.profilPanel.Controls.Add(this.addressTextBox);
            this.profilPanel.Controls.Add(this.emailLabel);
            this.profilPanel.Controls.Add(this.phoneTextbox);
            this.profilPanel.Controls.Add(this.newPasswordLabel);
            this.profilPanel.Controls.Add(this.nameTextBox);
            this.profilPanel.Controls.Add(this.phoneLabel);
            this.profilPanel.Controls.Add(this.newPasswordTextBox);
            this.profilPanel.Controls.Add(this.addressLabel);
            this.profilPanel.Controls.Add(this.newPassword2Label);
            this.profilPanel.Controls.Add(this.oldPasswordLabel);
            this.profilPanel.Location = new System.Drawing.Point(3, 0);
            this.profilPanel.Name = "profilPanel";
            this.profilPanel.Size = new System.Drawing.Size(337, 453);
            this.profilPanel.TabIndex = 1;
            this.profilPanel.Visible = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "\"kép|*.jpg;*.png;*.bmp\"";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // SearchInputFlowLayoutPanel
            // 
            this.SearchInputFlowLayoutPanel.Controls.Add(this.flowLayoutPanel1);
            this.SearchInputFlowLayoutPanel.Controls.Add(this.flowLayoutPanel2);
            this.SearchInputFlowLayoutPanel.Controls.Add(this.flowLayoutPanel3);
            this.SearchInputFlowLayoutPanel.Controls.Add(this.flowLayoutPanel6);
            this.SearchInputFlowLayoutPanel.Controls.Add(this.flowLayoutPanel8);
            this.SearchInputFlowLayoutPanel.Controls.Add(this.flowLayoutPanel7);
            this.SearchInputFlowLayoutPanel.Location = new System.Drawing.Point(3, 47);
            this.SearchInputFlowLayoutPanel.Name = "SearchInputFlowLayoutPanel";
            this.SearchInputFlowLayoutPanel.Size = new System.Drawing.Size(205, 316);
            this.SearchInputFlowLayoutPanel.TabIndex = 21;
            // 
            // OwnAuctionsFlowLayoutPanel
            // 
            this.OwnAuctionsFlowLayoutPanel.Controls.Add(this.OwnAuctionsButton);
            this.OwnAuctionsFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.OwnAuctionsFlowLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.OwnAuctionsFlowLayoutPanel.Name = "OwnAuctionsFlowLayoutPanel";
            this.OwnAuctionsFlowLayoutPanel.Size = new System.Drawing.Size(200, 38);
            this.OwnAuctionsFlowLayoutPanel.TabIndex = 22;
            // 
            // OwnAuctionsButton
            // 
            this.OwnAuctionsButton.Location = new System.Drawing.Point(3, 3);
            this.OwnAuctionsButton.Name = "OwnAuctionsButton";
            this.OwnAuctionsButton.Size = new System.Drawing.Size(95, 23);
            this.OwnAuctionsButton.TabIndex = 0;
            this.OwnAuctionsButton.Text = "Frissít";
            this.OwnAuctionsButton.UseVisualStyleBackColor = true;
            this.OwnAuctionsButton.Click += new System.EventHandler(this.OwnAuctionsButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1200, 800);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.headPanel);
            this.Controls.Add(this.sideMenuPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.SearchFlowLayoutPanel.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.flowLayoutPanel6.ResumeLayout(false);
            this.flowLayoutPanel6.PerformLayout();
            this.flowLayoutPanel8.ResumeLayout(false);
            this.flowLayoutPanel8.PerformLayout();
            this.flowLayoutPanel7.ResumeLayout(false);
            this.sideMenuPanel.ResumeLayout(false);
            this.sideMenuPanel.PerformLayout();
            this.headPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.menuBTN)).EndInit();
            this.mainPanel.ResumeLayout(false);
            this.profilPanel.ResumeLayout(false);
            this.profilPanel.PerformLayout();
            this.SearchInputFlowLayoutPanel.ResumeLayout(false);
            this.OwnAuctionsFlowLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.CheckBox AuctionSearchMyCheckBox;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.Button AuctionSearchButton;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox AuctionSearchDescriptionTextBox;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox AuctionSearchPriceTextBox;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox AuctionSearchCategoryTextBox;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox AuctionSearchNameTextBox;
        private System.Windows.Forms.TextBox oldPasswordTextBox;
        private System.Windows.Forms.TextBox newPassword2TextBox;
        private System.Windows.Forms.TextBox addressTextBox;
        private System.Windows.Forms.TextBox phoneTextbox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox newPasswordTextBox;
        private System.Windows.Forms.TextBox emailTextBox;
        private System.Windows.Forms.Label oldPasswordLabel;
        private System.Windows.Forms.Label newPassword2Label;
        private System.Windows.Forms.Button confirmButton;
        private System.Windows.Forms.Label addressLabel;
        private System.Windows.Forms.Label phoneLabel;
        private System.Windows.Forms.Label newPasswordLabel;
        private System.Windows.Forms.Label emailLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.FlowLayoutPanel SearchFlowLayoutPanel;
        private System.Windows.Forms.ListBox AuctionSearchListBox;
        private System.Windows.Forms.Button SearchCreateAction;
        private System.Windows.Forms.Panel sideMenuPanel;
        private System.Windows.Forms.Panel headPanel;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.PictureBox menuBTN;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button productsButton;
        private System.Windows.Forms.Button profilButton;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button ownProductsButton;
        private System.Windows.Forms.Button startPageButton;
        private System.Windows.Forms.Panel profilPanel;
        private System.Windows.Forms.FlowLayoutPanel SearchInputFlowLayoutPanel;
        private System.Windows.Forms.FlowLayoutPanel OwnAuctionsFlowLayoutPanel;
        private System.Windows.Forms.Button OwnAuctionsButton;
    }
}