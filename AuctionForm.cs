﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic;
using System.Drawing.Imaging;

namespace AukcioKliens
{
    public partial class AuctionForm : Form
    {
        Auction a;

        private void CreateHeaders(string endpoint)
        {
            WebClient.Client.DefaultRequestHeaders.Clear();
            WebClient.Client.DefaultRequestHeaders.Add("x-jwt-token", WebClient.token);
            WebClient.Client.DefaultRequestHeaders.Add("x-endpoint-name", endpoint);
        }

        private StringContent CreateGetIdContent(int id)
        {
            var userIDObject = new
            {
                Id = id
            };
            string stringcontent = JsonConvert.SerializeObject(userIDObject);
            return new StringContent(stringcontent, Encoding.UTF8, "application/json");
        }
        public AuctionForm(Auction otherclass)
        {
            a = otherclass;
            InitializeComponent();
            AuctionQuestionListBox.DrawMode = DrawMode.OwnerDrawFixed;
            AuctionQuestionListBox.ItemHeight = (int)(PictureHeight / 2 + 2 * ItemMargin);
            AuctionQuestionListBox.DrawItem += new DrawItemEventHandler(listBox2_DrawItem);
            AuctionQuestionListBox.MeasureItem += new MeasureItemEventHandler(listBox2_MeasureItem);
            if (a.Picture==null)
            {
                CreateHeaders("getpicture");
                var result2 = WebClient.Client.PutAsync("items", CreateGetIdContent(a.Id)).Result;
                var contentString2 = result2.Content.ReadAsStringAsync().Result;
                contentString2= contentString2!=""?contentString2: "/9j/4AAQSkZJRgABAQAAAQABAAD/7gAOQWRvYmUAZMAAAAAB/9sAQwAQCwsLDAsQDAwQFw8NDxcbFBAQFBsfFxcXFxcfHhcaGhoaFx4eIyUnJSMeLy8zMy8vQEBAQEBAQEBAQEBAQEBA/9sAQwERDw8RExEVEhIVFBEUERQaFBYWFBomGhocGhomMCMeHh4eIzArLicnJy4rNTUwMDU1QEA/QEBAQEBAQEBAQEBA/8AAEQgB8QOEAwEiAAIRAQMRAf/EABkAAQEBAQEBAAAAAAAAAAAAAAAEAwIBBv/EACsQAQABAgQEBgMBAQEAAAAAAAABAgMREzFRBDJhcRIUIUFSgSIzkUKhsf/EABQBAQAAAAAAAAAAAAAAAAAAAAD/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwD7oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHk1UxrMQD0cTetx74uZ4in2iZBqJ54ir2iIczduT7/wFQjmZnWcVcTjETuD0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAczXRGtUOZv0RvINBjPEbU/1zN+udMIBQ8mYjX0SzcrnWqXgKZu24/05m/R7RMpwG08RPtDmb1yffBm9B7NdU6zLl1FuudKZdxYrnXCAZDeOH3q/jqLFEdQTPVUUURpEOL8fhjtIJ1VqcbcJVHDz+MxtINQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZTfiPSIcTfq9oiHNyMK6o6uAdzduT7/wAczMzrOJETOkYuotXJ9v6Dgaxw9XvMQ6jh6feZkGAqizbj2x7uoppjSIgEsU1TpEy6izcn2w7qQGEcPPvLqLFHvMy1AcRatx/l1ERGno9AAAAAHNyMaKo6OifWMARNuHn1mGU+k4O7M4XI6+gKQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAT34wrx3hk34iOWfpgCymcaYneHrizONuOno7AeYxu9SXOeruCrxU7weKneEYCzxU7weKneEYCzxU7weKneEYCzxU7weKneEYCzxU7weKneEYCzxU7weKneEYCzxU7weKneEYDq56Vz3KZwqidpcgLPFTvB4qd4RgLPFTvB4qd4RgLImJ0eseH0nu2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABnfjG32TK64xomOiQG/Dz+MxtLZPYn8pjeFACS5z1d1aS5z1dweRGM4bu8i50c080d4VgmyLnQyLnRvmUfJ5m2/lAMci50Mi50bZtv5QZtv5QDHIudDIudG2bb+UGbb+UAxyLnQyLnRtm2/lBm2/lAMci50Mi50bZtv5QZtv5QDHIudDIudG2bb+UGbb+UAxyLnQyLnRtm2/lBm2/lAMci50Mi50bZtv5Q6pqpq0nEE1VqqmMZ0cKb/6/tMDfh9J7tmPD6T3bAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAI5jCZjZYluxhckC1OFyP4qRxOExOywBJc56u6tJc56u4FPNHeFaSnmjvCsEc6vHs6vAHXhq1wnDs3tW4piJnmn/jQEQpu24qiao5o/wCpgAAAAAAAAG3D61MW3D61A7v/AK/tMpv/AK/tMDfh9J7tmPD6T3bAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJ+Ij8oneFDLiI/GJ2kE6uicaInokU2Jxo7SDRJc56u6tJc56u4FPNHeFaSnmjvCsEc6hOrwFoxtXYiPDV9S18VOuMA9Rtrt2Jjw0/csAAAAegREzOEatfLzhr6+8O7VvwxjPN/40BHMTE4TrDxTdt+OMY5oTg8bcPrUxbcPrUDu/8Ar+0ym/8Ar+0wN+H0nu2Y8PpPdsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4vRjbnp6u3lUY0zG8Ajb8PPNH2waWJwrw3gFKS5z1d1aS5z1dwKeaO8K0lPNHeFYI51ePZ1eAAAAAAAKLVrD8qtfaHNm1/qr6huAAAyvW8fyp194agIm3D61F63h+VOnvBw+tQO7/6/tMpv/r+0wN+H0nu2Y8PpPdsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACSqMKpjqW5wrpnq6vRhcnq4BYkuc9XdVE4xE7pbnPV3Ap5o7wrSU80d4VgjnV49nV4ANKLVVfrpG7em3TTGER3BINq7HvR/GUxMekg8a2rXi/KrT2jd5at+KcZ5Y/6pAAAAAAAcUW/BVMxpLsBnf/AF/aZTf/AF/aYG/D6T3bMeH0nu2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABhxEesSxUX4/CJ2lOCq1ONuE9znq7trE/hMbSxuc9XcCnmjvCtJTzR3hWCPCZnCPWW9FmI9avWdndNNNOkOgAAHNVFNesfboB5EREYRo9AAAAAAAAAGd/9f2mU3/1/aYG/D6T3bMeH0nu2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABxdjG3UlWTGMTG6QCJmNJweOooqnSJeTExOE6g9p5o7wrRxOExOzbzEbSDYY+YjaTzEbSDYY+YjaTzEbSDYY+YjaTzEbSDYY+YjaTzEbSDYY+YjaTzEbSDYY+YjaTzEbSDYY+YjaTzEbSDYY+YjaTzEbSDq/wDr+0zW5diunDDBkDfh9J7tmPD6T3bAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPIiI0jB6AJLnPV3VuJtUTOMx6yCUU5NvYybewJhTk29jJt7AmFOTb2Mm3sCYU5NvYybewJhTk29jJt7AmFOTb2Mm3sCYU5NvYybewJhTk29jJt7AmFOTb2Mm3sCYU5NvYybewOeH0nu2c00U08vu6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//2Q==";
                var imgArr = Convert.FromBase64String(contentString2);
                using (var ms = new MemoryStream(imgArr))
                {
                    a.Picture = Image.FromStream(ms);
                }
            }
        }

        public AuctionForm()
        {
            InitializeComponent();
            AuctionQuestionListBox.DrawMode = DrawMode.OwnerDrawFixed;
            AuctionQuestionListBox.ItemHeight = (int)(PictureHeight / 2 + 2 * ItemMargin);
            AuctionQuestionListBox.DrawItem += new DrawItemEventHandler(listBox2_DrawItem);
            AuctionQuestionListBox.MeasureItem += new MeasureItemEventHandler(listBox2_MeasureItem);
        }

        [DllImport("user32.dll", EntryPoint = "ReleaseCapture")]
        private static extern void ReleaseCapture();
        [DllImport("user32.dll", EntryPoint = "SendMessage")]
        private static extern void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        public class ItemCreateData
        {
            public string Name { get; set; }
            public string Description { get; set; }
            public string Picture { get; set; }
            public string ThumbnailPicture { get; set; }
            public int HowManyDays { get; set; }
            public int Minprice { get; set; }
            public string Category { get; set; }
        }
        public class Bid
        {
            public int ItemId { get; set; }
            public int MaxBid { get; set; }
        }
        class Message
        {
            public string Name = "";
            public string Text { get; set; } = "";
            public int ItemId { get; set; }
            public DateTime Time { get; set; }
            public bool CreateOne { get; set; } = false;
            public Message(string name, string text)
            {
                Name = name;
                Text = text;
            }
        }

        private const int ItemMargin = 5;
        private const float PictureHeight = 100f;

        private void auctionForm_Load(object sender, EventArgs e)
        {

        }

        public void AuctionPanelInit()
        {
            AuctionFeedbackFlowLayoutPanel.Visible = true;
            AuctionFeedbackFlowLayoutPanel.Enabled = true;
            AuctionFeedbackFlowLayoutPanel.Visible = true;
            AuctionFeedbackFlowLayoutPanel.Enabled = true;
            AuctionLicit1FlowLayoutPanel.Visible = true;
            AuctionLicit1FlowLayoutPanel.Enabled = true;
            AuctionLicit2FlowLayoutPanel.Visible = true;
            AuctionLicit2FlowLayoutPanel.Enabled = true;
            AuctionStartDateFlowLayoutPanel.Enabled = true;
            AuctionQuestionFlowLayoutPanel.Visible = true;
            AuctionQuestionFlowLayoutPanel.Enabled = true;
            EmailFlowLayoutPanel.Visible = false;
            EmailFlowLayoutPanel.Enabled = false;
            AuctionBidButton.Enabled = true;
            AuctionBidButton.Visible = true;
            AuctionWinnerFlowLayoutPanel.Visible = true;
            AuctionWinnerFlowLayoutPanel.Enabled = true;
            AuctionGetPictureFlowLayoutPanel.Visible = true;
            AuctionGetPictureFlowLayoutPanel.Enabled = true;
            AuctionSendFeedbackButton.Visible = true;
            AuctionSendFeedbackButton.Enabled = true;
            AuctionSaveButton.Enabled = true;
            AuctionSaveButton.Visible = true;
            AuctionQuestionListBox.Enabled = true;
            label13.Text = "Jelenljegi ár:";

            AuctionBidTextBox.Text = "";
            AuctionBidOwnerTextBox.Text = "";
            AuctionCategoryTextBox.Text = "";
            AuctionDescriptionTextBox.Text = "";
            AuctionMinPriceTextBox.Text = "";
            AuctionNameTextBox.Text = "";
            AuctionPictureBox.Image = null;
            AuctionPriceNowTextBox.Text = "";
            AuctionQuestionListBox.Items.Clear();
            AuctionFeedbackDescriptionTextBox.Text = "";
            AuctionFeedbackPointComboBox.Text = "";
            AuctionMAxBidTextBox.Text = "";
            AuctionQuestionListBox.Items.Clear();

            AuctionBidTextBox.Enabled = true;
            AuctionBidOwnerTextBox.Enabled = true;
            AuctionCategoryTextBox.Enabled = true;
            AuctionDescriptionTextBox.Enabled = true;
            AuctionMinPriceTextBox.Enabled = true;
            AuctionNameTextBox.Enabled = true;
            AuctionPictureBox.Image = null;
            AuctionPriceNowTextBox.Enabled = true;
            AuctionQuestionListBox.Items.Clear();
            AuctionFeedbackDescriptionTextBox.Enabled = true;
            AuctionFeedbackPointComboBox.Enabled = true;
            AuctionStartDateTimePicker.Enabled = true;
            AuctionEndDateTimePicker.Enabled = true;
        }

        public void ShowAuctionBid()
        {
            AuctionGetPictureFlowLayoutPanel.Visible = false;
            AuctionGetPictureFlowLayoutPanel.Enabled = false;
            if (a.EndDate <= DateTime.Now)
            {
                AuctionBidButton.Enabled = false;
                AuctionBidButton.Visible = false;
                AuctionLicit2FlowLayoutPanel.Visible = false;
                AuctionLicit2FlowLayoutPanel.Enabled = false;
                AuctionQuestionListBox.Enabled = false;
                EmailFlowLayoutPanel.Visible = true;
                if (a.OwnerId==WebClient.currentUserID)
                {
                    string ownersting = "users";// + a.WinnerId.ToString();
                    CreateHeaders("getuser");
                    var result2 = WebClient.Client.PostAsync(ownersting,CreateGetIdContent((int)a.WinnerId)).Result;
                    var contentString2 = result2.Content.ReadAsStringAsync().Result;
                    string email="";
                    try
                    {
                        var json2 = JsonObject.Parse(contentString2);
                        email = json2["email"];
                    }
                    catch (Exception e) { }
                    EmailTextBox.Text = email;
                    EmailLabel.Text = "Vevő email címe:";
                }
                else if (a.WinnerId==WebClient.currentUserID)
                {
                    string ownersting = "users" + a.OwnerId.ToString();
                    CreateHeaders("getuser");
                    var result2 = WebClient.Client.PostAsync(ownersting,CreateGetIdContent(a.OwnerId)).Result;
                    var contentString2 = result2.Content.ReadAsStringAsync().Result;
                    string email = "";
                    try
                    {
                        var json2 = JsonObject.Parse(contentString2);
                        email = json2["email"];
                    }
                    catch (Exception e) { }
                    EmailTextBox.Text = email;
                    EmailLabel.Text = "Eladó email címe:";
                }
            }
            else 
            {
                if (a.MaxBid != null)
                {
                    AuctionMAxBidTextBox.Text = a.MaxBid.ToString();
                }
                var temp = GetMessages(a.Id);
                foreach (var el in temp)
                    AuctionQuestionListBox.Items.Add(el);
                Message m = new Message("", "");
                m.CreateOne = true;
                AuctionQuestionListBox.Items.Add(m);
            }
            if (WebClient.currentUserID == a.OwnerId)
            {
                AuctionBidButton.Enabled = false;
                AuctionBidButton.Visible = false;
                AuctionLicit2FlowLayoutPanel.Visible = false;
                AuctionLicit2FlowLayoutPanel.Enabled = false;
            }
            AuctionSaveButton.Enabled = false;
            AuctionSaveButton.Visible = false;
            AuctionSendFeedbackButton.Enabled = false;
            AuctionSendFeedbackButton.Visible = false;
            AuctionBidOwnerTextBox.Text = a.BidOwner;
            AuctionBidOwnerTextBox.Enabled = false;
            AuctionCategoryTextBox.Text = a.Category;
            AuctionCategoryTextBox.Enabled = false;
            AuctionDescriptionTextBox.Text = a.Description;
            AuctionDescriptionTextBox.Enabled = false;
            AuctionMinPriceTextBox.Text = a.Minprice.ToString();
            AuctionMinPriceTextBox.Enabled = false;
            AuctionNameTextBox.Text = a.Name;
            AuctionNameTextBox.Enabled = false;
            AuctionPictureBox.Image = a.Picture;
            AuctionPriceNowTextBox.Text = a.Price.ToString();
            AuctionPriceNowTextBox.Enabled = false;
            AuctionStartDateTimePicker.Value = a.StartDate;
            AuctionStartDateTimePicker.Enabled = false;
            AuctionEndDateTimePicker.MinDate = a.EndDate;
            AuctionEndDateTimePicker.Value = a.EndDate;
            AuctionEndDateTimePicker.Enabled = false;


           if (a.EndDate <= DateTime.Now && a.Star == null && a.WinnerId == WebClient.currentUserID)
            {
                AuctionFeedbackDescriptionTextBox.Text = "";
                AuctionFeedbackPointComboBox.Text = "";
                AuctionSendFeedbackButton.Enabled = true;
                AuctionSendFeedbackButton.Visible = true;
            }
            else if (a.EndDate <= DateTime.Now && a.Star != null)
            {
                AuctionFeedbackDescriptionTextBox.Text = a.StarDescription;
                AuctionFeedbackDescriptionTextBox.Enabled = false;
                AuctionFeedbackPointComboBox.Text = a.Star.ToString();
                AuctionFeedbackPointComboBox.Enabled = false;
            }
            else
            {
                AuctionFeedbackFlowLayoutPanel.Visible = false;
                AuctionFeedbackFlowLayoutPanel.Enabled = false;
            }
        }
        private void listBox2_DrawItem(object sender, DrawItemEventArgs e)
        {
            // Get the ListBox and the item.
            ListBox lst = sender as ListBox;
            
            if (lst.Items.Count > 0)
            {
                if (e.Index >= 0)
                {
                    Message message = (Message)lst.Items[e.Index];

                    // Draw the background.
                    e.DrawBackground();


                    // See if the item is selected.
                    Brush br;
                    if ((e.State & DrawItemState.Selected) ==
                        DrawItemState.Selected)
                        br = SystemBrushes.HighlightText;
                    else
                        br = new SolidBrush(e.ForeColor);
                    // Find the area in which to put the text.
                    float x = e.Bounds.Left + ItemMargin;
                    float y = e.Bounds.Top + ItemMargin;
                    float width = e.Bounds.Right - ItemMargin - x;
                    float height = e.Bounds.Bottom - ItemMargin - y;
                    RectangleF layout_rect1 = new RectangleF(x, y, width, height);
                    // Draw the text.
                    Font f = new Font("Microsoft Sans Serif", 11, FontStyle.Regular);
                    string txt = "Név:" + "     " + message.Name + '\n' + "Üzenet:" + "     " + message.Text;
                    if (message.CreateOne)
                        txt = "Új hozzászólás";
                    e.Graphics.DrawString(txt, f, br, layout_rect1);

                    // Outline the text.
                    e.Graphics.DrawRectangle(Pens.Black,
                        Rectangle.Round(layout_rect1));

                    // Draw the focus rectangle if appropriate.
                    e.DrawFocusRectangle();
                }
            }
        }
        private void listBox2_MeasureItem(object sender, MeasureItemEventArgs e)
        {
            e.ItemHeight = (int)(PictureHeight / 2 + 2 * ItemMargin);
        }
        private void AuctionBidButton_Click(object sender, EventArgs e)
        {
            bool pricegood = true;
            int newprice = -1;
            int oldprice = 0;
            try
            {
                oldprice = Convert.ToInt32(AuctionPriceNowTextBox.Text);
                newprice = Convert.ToInt32(AuctionBidTextBox.Text);
            }
            catch (Exception)
            {
                pricegood = false;
            }

            if (pricegood && newprice > oldprice)
            {
                Bid b = new Bid();
                b.ItemId = a.Id;
                b.MaxBid = newprice;
                var content = new StringContent(JsonConvert.SerializeObject(b), Encoding.UTF8, "application/json");
                CreateHeaders("placebid");
                var result = WebClient.Client.PostAsync("bid", content).Result;
                var contentString = result.Content.ReadAsStringAsync().Result;
                //var json = JsonObject.Parse(contentString);
                if (result.IsSuccessStatusCode)
                {
                    MessageBox.Show("Sikeres Licit!");
                }
                else
                {
                    MessageBox.Show("Túl licitáltak!");
                }
                CreateHeaders("getitem");
                var result2 = WebClient.Client.PostAsync("items", CreateGetIdContent(a.Id)).Result;
                var contentString2 = result2.Content.ReadAsStringAsync().Result;
                var json2 = JsonObject.Parse(contentString2);
                a.Price = json2["Price"];
                int? maxbid= json2["MaxBid"]?? 0;
                if (maxbid!=0)
                    a.MaxBid = maxbid;
                a.BidOwner = "";
                if (json2["BidOwnerId"] != null)
                {

                    string ownersting3 = "users";// + json2["bidOwnerId"].ToString();
                    CreateHeaders("getuser");
                    var result3 = WebClient.Client.PostAsync(ownersting3, CreateGetIdContent((int)json2["BidOwnerId"])).Result;
                    var contentString3 = result3.Content.ReadAsStringAsync().Result;
                    string owner2 = "";
                    try
                    {
                        var json3 = JsonObject.Parse(contentString3);
                        owner2 = json3["Name"];
                    }
                    catch (Exception) { }

                    a.BidOwner = owner2;
                }
                AuctionBidOwnerTextBox.Text = a.BidOwner;
                AuctionMAxBidTextBox.Text = "";
                if (a.MaxBid!=0)
                AuctionMAxBidTextBox.Text = a.MaxBid.ToString();
                AuctionPriceNowTextBox.Text = a.Price.ToString();
            }
            else
            {
                MessageBox.Show("Hibás licit!");
            }
        }
        public void ShowAuctionCreate()
        {
            AuctionFeedbackFlowLayoutPanel.Visible = false;
            AuctionFeedbackFlowLayoutPanel.Enabled = false;
            AuctionLicit2FlowLayoutPanel.Visible = false;
            AuctionLicit2FlowLayoutPanel.Enabled = false;
            AuctionStartDateFlowLayoutPanel.Enabled = false;
            AuctionStartDateTimePicker.Value = DateTime.Now;
            AuctionQuestionFlowLayoutPanel.Visible = false;
            AuctionQuestionFlowLayoutPanel.Enabled = false;
            AuctionBidButton.Enabled = false;
            AuctionBidButton.Visible = false;
            AuctionWinnerFlowLayoutPanel.Visible = false;
            AuctionWinnerFlowLayoutPanel.Enabled = false;
            AuctionPriceNowTextBox.Text = "0";
            AuctionPriceNowTextBox.Enabled = false;
            label13.Text = "Kezdő ár:";
            AuctionEndDateTimePicker.MinDate = DateTime.Now.AddDays(7);
        }

        private void AuctionSaveButton_Click(object sender, EventArgs e)
        {
            bool pricegood = true;
            ItemCreateData i = new ItemCreateData();
            i.Category = AuctionCategoryTextBox.Text;
            i.Description = AuctionDescriptionTextBox.Text;
            i.HowManyDays = (int)(AuctionEndDateTimePicker.Value - AuctionStartDateTimePicker.Value).TotalDays;
            i.Name = AuctionNameTextBox.Text;
            int minprice = -1;
            int price = -1;
            try
            {
                minprice = Convert.ToInt32(AuctionMinPriceTextBox.Text);
                price = Convert.ToInt32(AuctionPriceNowTextBox.Text);
            }
            catch (Exception)
            {
                pricegood = false;
            }
            if (pricegood && price <= minprice && price >= 0)
            {
                i.Minprice = minprice;
            }
            else
                pricegood = false;
            Image image = AuctionPictureBox.Image;
            Image thimage = ThumbnailCreater(image);
            string s = "";
            if (image != null)
                s = ImagetoBase64(image);
            i.Picture = s;
            s = "";
            if (thimage != null)
                s = ImagetoBase64(thimage);
            i.ThumbnailPicture = s;
            if (i.Category != "" && i.Description != "" && i.HowManyDays >= 6 && pricegood && i.Name != "" && image != null)
            {
                var content = new StringContent(JsonConvert.SerializeObject(i), Encoding.UTF8, "application/json");
                CreateHeaders("createitem");
                var result = WebClient.Client.PutAsync("items", content).Result;
                if (result.IsSuccessStatusCode)
                {
                    MessageBox.Show("Sikeres hirdetésfeladás!");
                }
                else
                {
                    MessageBox.Show("Nem sikerült a feladás!");
                }
            }
            else
            {
                MessageBox.Show("Hibás adatokat adtál meg!");
            }
        }
        private string ImagetoBase64(Image image)
        {
            byte[] imageByteArray;
            using (var ms = new MemoryStream())
            {
                image.Save(ms, ImageFormat.Jpeg);
                imageByteArray = ms.ToArray();
            }
            return Convert.ToBase64String(imageByteArray);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Image openImage = Image.FromFile(openFileDialog1.FileName);
                AuctionPictureBox.Image = openImage;
           }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }
        class MessageCreateData
        {
            public int itemId { get; set; }
            public string text { get; set; }
        }
        private void AuctionQuestionListBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (AuctionQuestionListBox.Items.Count > 0)
            {
                ListBox l = (ListBox)sender;
                Message m = (Message)l.SelectedItem;
                if (l.SelectedIndex == l.Items.Count - 1)
                {
                    string text = Interaction.InputBox("Szöveg", "Új hozzászólás", "", -1, -1);
                    if (text != "")
                    {
                        CreateHeaders("createmessage");
                        MessageCreateData datas = new MessageCreateData
                        {
                            itemId =a.Id,
                            text = text
                        };
                        var content = new StringContent(JsonConvert.SerializeObject(datas), Encoding.UTF8, "application/json");
                        var result = WebClient.Client.PutAsync("messages", content).Result;
                        if (result.IsSuccessStatusCode)
                        {
                            MessageBox.Show("Sikeres hozzászólás!");
                        }
                        else
                        {
                            MessageBox.Show("Nem sikerült a hozzászólás!");
                        }
                    }
                    else
                        MessageBox.Show("Hibás adatot adtál meg!");
                    AuctionQuestionListBox.Items.Clear();
                    var temp = GetMessages(a.Id);
                    foreach(var el in temp)
                        AuctionQuestionListBox.Items.Add(el);
                    Message m2 = new Message("", "");
                    m2.CreateOne = true;
                    AuctionQuestionListBox.Items.Add(m2);
                }

            }
        }

        private void AuctionSendFeedbackButton_Click(object sender, EventArgs e)
        {
            int star = -1;
            try
            {
                star = Convert.ToInt32(AuctionFeedbackPointComboBox.Text);
            }
            catch (Exception)
            {
            }
            if (AuctionFeedbackDescriptionTextBox.Text!="" && (star>0 && star<6))
            {
                ItemRating f = new ItemRating();
                f.ItemId = a.Id;
                f.StarDescription = AuctionFeedbackDescriptionTextBox.Text;
                f.Stars = star;
                var content = new StringContent(JsonConvert.SerializeObject(f), Encoding.UTF8, "application/json");
                CreateHeaders("rateitem");
                var result = WebClient.Client.PostAsync("items", content).Result;
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    MessageBox.Show("Sikeres értékelés!");
                    a.StarDescription = AuctionFeedbackDescriptionTextBox.Text;
                    a.Star = star;
                    AuctionFeedbackFlowLayoutPanel.Enabled = false;
                }
                else
                    MessageBox.Show("Sikertelen értékelés!");
            }
            else 
            {
                MessageBox.Show("Hibás adatokat adtál meg!");
            }
        }

        private List<Message> GetMessages(int id)
        {
            List<Message> result = new List<Message>();
            ///-------------------------------------------------------------------------
            string ownersting = "messages";
            CreateHeaders("getmessages");
            var result2 = WebClient.Client.PostAsync(ownersting, CreateGetIdContent(id)).Result;
            var contentString = result2.Content.ReadAsStringAsync().Result;
            if (contentString != "")
            { 
                dynamic json = JsonConvert.DeserializeObject(contentString);
                foreach (var item in json)
                {
                    string text = item.Text;
                    string ownersting2 = "users";// + item.userId;
                    CreateHeaders("getuser");
                    var result3 = WebClient.Client.PostAsync(ownersting2, CreateGetIdContent((int)item.UserId)).Result;
                    var contentString3 = result3.Content.ReadAsStringAsync().Result;
                    string owner2 = "";
                    try
                    {
                        var json3 = JsonObject.Parse(contentString3);
                        owner2 = json3["Name"];
                    }
                    catch (Exception) { }
                    Message m1 = new Message(owner2, text);
                    string dateString = item.Time;
                    m1.Time = DateTime.Parse(dateString);
                    result.Add(m1);
                }
            }
            //-----------------------------------------------
            result = result.OrderBy(o => o.Time).ToList();
            return result;
        }

        public class ItemRating
        {
            public int ItemId { get; set; }
            public int Stars { get; set; }
            public string StarDescription { get; set; }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            pictureBox1.BackColor = Color.DarkRed;
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            pictureBox1.BackColor = pictureBox1.Parent.BackColor;
        }
        private void pictureBox2_MouseEnter(object sender, EventArgs e)
        {
            pictureBox2.BackColor = Color.DarkRed;
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            pictureBox2.BackColor = pictureBox1.Parent.BackColor;
        }

        private void AuctionForm_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void AuctionFlowLayoutPanel_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panels_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private Image ThumbnailCreater(Image i)
        {
            double scale = ((double)i.Height) / ((double)i.Width);
            scale = 100 * scale;
            Image th;
            if (scale < 100)
            {
                th = i.GetThumbnailImage(100, (int)scale, () => false, IntPtr.Zero);
            }
            else
            {
                th = i.GetThumbnailImage((int)scale, 100, () => false, IntPtr.Zero);
            }
            return th;
        }
    }
}
