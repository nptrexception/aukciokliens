﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AukcioKliens
{
    class WebClient
    {
        private static readonly object creationLock = new object();
        private static HttpClient instance = null;
        public static int currentUserID { get; set; } = -1;
        public System.Net.WebHeaderCollection Headers { get; set; }
        public static string token { get; set; }

        public static HttpClient Client
        {
            get
            {
                lock (creationLock)
                {
                    if (instance == null)
                    {
                        currentUserID = -1;
                        instance = new HttpClient();
                        instance.BaseAddress = new Uri("https://europe-west3-cloudprogmsc.cloudfunctions.net/");
                        //instance.BaseAddress = new Uri("http://localhost:8080");
                    }
                }
                return instance;
            }
        }
    }
}
